#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <time.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

// Definisi awalan untuk direktori modular
#define MODULAR_PREFIX "module_"
#define DIR_PATH "/home/znafian/Praktikum-Modul-4_Sisop-2023"
#define LOG_PATH "/home/znafian/Praktikum-Modul-4_Sisop-2023/fs_module.log"
#define XMP_NAMESPACE "http://example.org/fs_module"
#define MAX_DESC_LENGTH 2048

char log_path[1000] = LOG_PATH;
char xmp_namespace[1000] = XMP_NAMESPACE;
char dirpath[1000] = DIR_PATH;

void log_system_call(const char *level, const char *cmd, const char *desc);
void splitFileBesar(const char *filepath);
int combineFileKecil(const char *filepath, char *buf, size_t size);

void modularisasiDirektori(const char *dirpath) {
    DIR *dp;
    struct dirent *de;
    struct stat stbuf;

    dp = opendir(dirpath);
    if (dp == NULL) {
        perror("Gagal membuka direktori");
        return;
    }

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) {
            continue;
        }

        char fullpath[1000];
        sprintf(fullpath, "%s/%s", dirpath, de->d_name);

        if (de->d_type == DT_DIR) {
            modularisasiDirektori(fullpath);

            char desc[1000];
            sprintf(desc, "%s", fullpath);
            log_system_call("REPORT", "MODULARIZE_DIR", desc);

            if (strstr(de->d_name, MODULAR_PREFIX) == NULL) {
                char newname[1000];
                sprintf(newname, "%s/%s%s", dirpath, MODULAR_PREFIX, de->d_name);
                rename(fullpath, newname);
            }
        } else {
            if (lstat(fullpath, &stbuf) == 0) {
                if (S_ISREG(stbuf.st_mode) && stbuf.st_size > 1024) {
                    splitFileBesar(fullpath);
                }
            }

            char desc[1000];
            sprintf(desc, "%s", fullpath);
            log_system_call("REPORT", "MODULARIZE_FILE", desc);

            char newname[1000];
            sprintf(newname, "%s/modular_%s", dirpath, de->d_name);
            rename(fullpath, newname);
        }
    }

    closedir(dp);
}

void splitFileBesar(const char *filepath) {
    FILE *original_file = fopen(filepath, "rb");
    if (original_file == NULL) {
        perror("Gagal membuka original file");
        return;
    }

    fseek(original_file, 0, SEEK_END);
    long file_size = ftell(original_file);
    fseek(original_file, 0, SEEK_SET);

    int num_parts = (file_size + 1023) / 1024;

    for (int i = 0; i < num_parts; i++) {
        char part_name[1000];
        sprintf(part_name, "%s.%03d", filepath, i);

        FILE *part_file = fopen(part_name, "wb");
        if (part_file == NULL) {
            perror("Gagal membuka part file");
            fclose(original_file);
            return;
        }

        char buffer[1024];
        int read_size = fread(buffer, 1, 1024, original_file);
        if (read_size < 0) {
            perror("Gagal membaca dari original file");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        if (read_size != 1024) {
            fprintf(stderr, "Error: Read size mismatch\n");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        int write_size = fwrite(buffer, 1, read_size, part_file);
        if (write_size < read_size) {
            perror("Gagal menulis ke part file");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        fclose(part_file);
    }

    fclose(original_file);
}

// Fungsi untuk membaca file dan menggabungkan file kecil menjadi file asli
int combineFileKecil(const char *filepath, char *buf, size_t size) {
    int total_read = 0;
    struct stat stbuf;

    for (int i = 0; ; i++) {
        char part_name[1000];
        sprintf(part_name, "%s.%03d", filepath, i);

        FILE *part_file = fopen(part_name, "rb");
        if (part_file == NULL) {
            break;
        }

        if (total_read < size) {
            int read_size = fread(buf + total_read, 1, size - total_read, part_file);
            total_read += read_size;
        }

        fclose(part_file);

        if (total_read >= size) {
            break;
        }
    }

    if (lstat(filepath, &stbuf) == 0 && total_read < stbuf.st_size) {
        FILE *original_file = fopen(filepath, "rb");
        if (original_file != NULL) {
            fseek(original_file, total_read, SEEK_SET);
            int remaining_read = fread(buf + total_read, 1, stbuf.st_size - total_read, original_file);
            total_read += remaining_read;
            fclose(original_file);
        }
    }

    return total_read;
}

// Fungsi untuk membuka dan menutup file log sistem
void openLogFile() {
    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }
    fclose(log_file);
}

// Fungsi untuk mencatat log sistem
void log_system_call(const char *level, const char *cmd, const char *desc) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20];
    strftime(timestamp, 20, "%Y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }

    fprintf(log_file, "%s::%04d%02d%02d-%02d:%02d:%02d::%s::%s\n", level,
        tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
        tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, cmd, desc);

    fclose(log_file);
}

// Fungsi untuk mencatat properti XMP pada file log
void set_xmp_property(const char *path, const char *level, const char *cmd) {
    xmlDocPtr doc;
    xmlNodePtr root, prop;

    doc = xmlParseFile(log_path);
    if (doc == NULL) {
        perror("Gagal parsing XMP file");
        exit(EXIT_FAILURE);
    }

    root = xmlDocGetRootElement(doc);
    if (root == NULL) {
        perror("Gagal getting root element");
        xmlFreeDoc(doc);
        exit(EXIT_FAILURE);
    }

    xmlNsPtr ns = xmlNewNs(root, BAD_CAST xmp_namespace, NULL);
    xmlSetNs(root, ns);

    prop = xmlNewChild(root, ns, BAD_CAST "LOG", NULL);
    xmlNewProp(prop, BAD_CAST "LEVEL", BAD_CAST level);
    xmlNewProp(prop, BAD_CAST "CMD", BAD_CAST cmd);
    xmlNewProp(prop, BAD_CAST "PATH", BAD_CAST path);

    xmlSaveFormatFile(log_path, doc, 1);

    xmlFreeDoc(doc);
}

// Fungsi untuk mencatat log MODULAR saat xmp_getattr pada level REPORT
static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;

    if (strstr(path, MODULAR_PREFIX) != NULL) {
        log_system_call("REPORT", "MODULAR", path);
    }

    res = lstat(path, stbuf);
    if (res == -1)
        return -errno;

    if (S_ISDIR(stbuf->st_mode) && strstr(path, MODULAR_PREFIX) != NULL) {
        modularisasiDirektori(path);

        char desc[1000];
        sprintf(desc, "%s", path);
        log_system_call("REPORT", "MODULARIZE_MKDIR", desc);
    }

    return 0;
}

// Fungsi untuk membaca direktori
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    char desc[MAX_DESC_LENGTH];

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (filler(buf, de->d_name, NULL, 0) != 0)
            break;

        snprintf(desc, sizeof(desc), "%s%s", fpath, de->d_name);
        size_t desc_length = strlen(desc);

        if (desc_length >= sizeof(desc)) {
            fprintf(stderr, "String melebihi batas buffer: %s\n", desc);
        }

        log_system_call("REPORT", "MODULAR", desc);


        int mod_path_len;
        char mod_path[1000];
        mod_path_len = snprintf(mod_path, sizeof(mod_path), "%s%s", fpath, de->d_name);

        if (mod_path_len >= sizeof(mod_path)) {
            // Handle the error.
            fprintf(stderr, "Error in snprintf for mod_path: %s\n", mod_path);
            continue;
        }

        if (strstr(de->d_name, MODULAR_PREFIX) != NULL) {
            modularisasiDirektori(mod_path);
        }
    }

    closedir(dp);
    return 0;
}

// Fungsi untuk me-rename direktori
static int xmp_rename(const char *oldpath, const char *newpath) {
    char buf[1000];
    size_t size = sizeof(buf);

    struct stat stbuf;
    if (lstat(oldpath, &stbuf) == -1) {
        return -errno;
    }

    if (!S_ISDIR(stbuf.st_mode)) {
        return 0; 
    }

    if ((stbuf.st_mode & S_IRWXU) != (S_IRWXU | S_IWUSR | S_IXUSR)) {
        return 0; 
    }

    if (strstr(oldpath, MODULAR_PREFIX) != NULL) {
        char desc[1000];
        sprintf(desc, "%s", oldpath);
        log_system_call("REPORT", "REVERT_MODULARIZE", desc);

        char newname[1000];
        int newname_len = snprintf(newname, sizeof(newname), "%s/%s", dirpath, oldpath + strlen(MODULAR_PREFIX));
        if (newname_len > sizeof(newname)) {
            perror("Error reverting modularization");
            return -errno;
        }

        combineFileKecil(newname, buf, size);
        log_system_call("REPORT", "REVERT_MODULARIZE_SUCCESS", desc);
    }

    if (strstr(newpath, MODULAR_PREFIX) != NULL) {
        modularisasiDirektori(newpath);

        char desc[1000];
        sprintf(desc, "%s", newpath);
        log_system_call("REPORT", "MODULARIZE_RENAME", desc);
    }

    return 0;
}

static int xmp_rmdir(const char *path) {
    int res;

    res = rmdir(path);
    if (res == -1)
        return -errno;

    log_system_call("RMDIR", path, "");

    return 0;
}

static int xmp_unlink(const char *path) {
    int res;

    res = unlink(path);
    if (res == -1)
        return -errno;

    log_system_call("UNLINK", path, "");

    return 0;
}

// Fungsi untuk membaca file
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int fd;
    int res;
    struct stat stbuf;

    (void)fi;

    fd = open(path, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    // Pengecekan apakah direktori memiliki awalan "module_" setelah membaca
    if (strstr(path, MODULAR_PREFIX) != NULL) {
        char desc[1000];
        sprintf(desc, "%s", path);
        log_system_call("REPORT", "DEMODULAR", desc);

        if (lstat(path, &stbuf) == 0) {
            // Memastikan izin dan kepemilikan tetap sama
            chmod(path, stbuf.st_mode);
            chown(path, stbuf.st_uid, stbuf.st_gid);

            // Log saat gagal membaca dari direktori modular
            if (res == -1) {
                char desc_unlink[1000];
                sprintf(desc_unlink, "%s", path);
                log_system_call("FLAG", "UNLINK", desc_unlink);
            }

            // Pemecahan file kecil setelah membaca
            char buf_clean[1024];
            combineFileKecil(path, buf_clean, sizeof(buf_clean));
        }
    }

    return res;
}

// Fungsi operasi file system Fuse
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .read = xmp_read,
};

// Fungsi utama
int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }
    fclose(log_file);

    return fuse_main_real(argc, argv, &xmp_oper, sizeof(xmp_oper), NULL);
}
