#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>

static int is_delete_dir(const char *path) {
    return strstr(path, "delete-") == path;
}

static int is_rev_dir(const char *path) {
    return strstr(path, "rev-") == path;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;

    res = lstat(path, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    (void) offset;
    (void) fi;

    DIR *dp;
    struct dirent *de;

    dp = opendir(path);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (is_delete_dir(de->d_name) || is_rev_dir(de->d_name)) {
            continue;
        }

        if (filler(buf, de->d_name, NULL, 0))
            break;
    }

    closedir(dp);
    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    if (is_rev_dir(to)) {
        char reversed_name[strlen(to) + 1];
        int i, j = 0;
        for (i = strlen(to) - 1; i >= 0; i--) {
            reversed_name[j++] = to[i];
        }
        reversed_name[j] = '\0';
        int res = rename(from, reversed_name);

        if (res == -1)
            return -errno;
    } else if (is_delete_dir(to)) {
        int res = unlink(from);

        if (res == -1)
            return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
