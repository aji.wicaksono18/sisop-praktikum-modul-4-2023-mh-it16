**Anggota Kelompok:**

| Nama | NRP |
| ------ | ------ |
|Rahmad Aji Wicaksono       |5027221034        |
|Alma Amira Dewani          |5027221054        |
|Zidny Ilman Na'fian        |5027221072        |

# Soal 1
Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital.

## Cara Pengerjaan
**A.** Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:
- Membuat folder dengan prefix "rev." Dalam folder ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. 
	Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" 
    Output: eTgj80q7Ut7eNhNVooBE.HEIC
- Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus.
    Ex: "mv coba-deh.jpg delete-foto/" 

```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

// Fungsi untuk membuat folder dengan prefix "delete."
static int create_delete_folder(const char *path) {
    if (strstr(path, "/delete-") != NULL) {
        mkdir(path, 0755);
    }
    return 0;
}

// Fungsi untuk membuat folder dengan prefix "rev."
static int create_reverse_folder(const char *path) {
    if (strstr(path, "/rev-") != NULL) {
        mkdir(path, 0755);
    }
    return 0;
}

// Fungsi untuk melakukan reverse pada nama file dalam folder dengan prefix "rev."
static int reverse_filename(const char *path) {
    char reversed_path[PATH_MAX];
    char *filename;
    strcpy(reversed_path, path);
    filename = strrchr(reversed_path, '/');
    if (filename != NULL) {
        filename++;
        strrev(filename);
        rename(path, reversed_path);
    }
    return 0;
}

int main(int argc, char *argv[]) {
    umask(0);
    return fuse_main(argc, argv, &my_operations, NULL);
}
```

1. Fungsi `create_delete_folder`
   - Membuat folder dengan prefix "delete." jika path mengandung "/delete-".
   - Fungsi ini menggunakan fungsi `strstr` untuk mencari apakah string "/delete-" ada dalam path.
   - Jika "/delete-" ditemukan, maka fungsi `mkdir` digunakan untuk membuat folder dengan hak akses 0755.

2. Fungsi `create_reverse_folder`
   - Membuat folder dengan prefix "rev." jika path mengandung "/rev-".
   - Fungsi ini juga menggunakan `strstr` untuk mencari apakah string "/rev-" ada dalam path.
   - Jika "/rev-" ditemukan, maka fungsi `mkdir` digunakan untuk membuat folder dengan hak akses 0755.

3. Fungsi `reverse_filename`
   - Melakukan reverse pada nama file dalam folder dengan prefix "rev.".
   - Fungsi ini menggunakan array `reversed_path` untuk menyimpan path yang akan di-reverse.
   - Menggunakan `strrchr` untuk mencari pointer ke karakter '/' terakhir dalam path, yang menandakan awal nama file.
   - Menggunakan `strrev` (yang diasumsikan ada di tempat lain dalam kode) untuk melakukan reverse pada nama file.
   - Menggunakan `rename` untuk mengganti nama file asli dengan nama file yang sudah di-reverse.

4. Fungsi `main`
   - Memulai eksekusi program.
   - Mengatur umask menjadi 0 menggunakan `umask(0)`.
   - Memanggil fungsi `fuse_main` dengan parameter argc, argv, pointer ke `my_operations`, dan NULL. `fuse_main` digunakan untuk menjalankan FUSE (Filesystem in Userspace).

## Kendala
- Linux error saat login kembali jadi harus install ulang lagi (jangan mount root!).
- Command tidak berjalan pada mount.

## Revisi
**B.** Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini. 
- Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."
- Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.

```c
// Fungsi untuk melakukan reverse pada isi file
void reverseFileContents(const char *inputFileName, const char *outputFileName) {
    FILE *inputFile = fopen(inputFileName, "r");
    FILE *outputFile = fopen(outputFileName, "w");

    if (inputFile == NULL || outputFile == NULL) {
        perror("Error opening files");
        exit(EXIT_FAILURE);
    }

    fseek(inputFile, 0, SEEK_END);
    long fileSize = ftell(inputFile);
    fseek(inputFile, 0, SEEK_SET);

    char *buffer = (char *)malloc(fileSize + 1);

    if (buffer == NULL) {
        perror("Error allocating memory");
        exit(EXIT_FAILURE);
    }

    fread(buffer, 1, fileSize, inputFile);
    buffer[fileSize] = '\0';

    for (long i = fileSize - 1; i >= 0; i--) {
        fputc(buffer[i], outputFile);
    }

    free(buffer);

    fclose(inputFile);
    fclose(outputFile);
}

int main() {
    // Mengubah permission pada file "script.sh"
    const char *scriptFileName = "sisop/script.sh";
    const char *galleryFolderName = "sisop/gallery";
    const char *tulisanFolderName = "sisop/tulisan";

    if (chmod(scriptFileName, 0700) == -1) {
        perror("Error changing file permission");
        exit(EXIT_FAILURE);
    }

    // Menghapus isi dari "gallery," "sisop," dan "tulisan"
    if (remove(galleryFolderName) == -1 || remove(tulisanFolderName) == -1) {
        perror("Error removing folders");
        exit(EXIT_FAILURE);
    }

    // Menambahkan fitur baru dengan membuat file "test_reverse.txt"
    const char *testFileName = "sisop/test_reverse.txt";
    reverseFileContents(scriptFileName, testFileName);
    return 0;
}
```
1. Fungsi `reverseFileContents`
   - Menerima dua parameter, `inputFileName` (nama file sumber) dan `outputFileName` (nama file target).
   - Membaca isi dari file sumber, membalikkan kontennya, dan menuliskannya ke file target.
   - Menggunakan fungsi `fopen` untuk membuka file, `fseek` untuk mendapatkan ukuran file, dan `fread` untuk membaca isi file ke dalam buffer.
   - Menggunakan alokasi dinamis (`malloc`) untuk buffer.
   - Setelah mengolah data, buffer dihapus dari memori menggunakan `free`.

2. Fungsi `main`
   - Mengubah permission pada file "script.sh" menjadi 0700 menggunakan fungsi `chmod`.
   - Menghapus isi dari dua folder, "gallery" dan "tulisan," menggunakan fungsi `remove`.
   - Membuat file baru "test_reverse.txt" di dalam folder "sisop" dengan isi yang merupakan kebalikan dari isi "script.sh" menggunakan fungsi `reverseFileContents`.

**C.** Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.
- Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.
- Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.
- Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.
- Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.
```c
// Fungsi untuk membalikkan string
void strrev(char *str) {
    int length = strlen(str);
    int i, j;
    char temp;

    for (i = 0, j = length - 1; i < j; ++i, --j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
    }
}

// Fungsi untuk mendekode isi file dengan algoritma Base64
char *base64decoder(char encoded[], int string_length) {
    char *decoded_string;
    decoded_string = (char *)malloc(sizeof(char) * 100);

    int i, j, k = 0;
    int num = 0;
    int count_bits = 0;

    for (i = 0; i < string_length; i += 4) {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++) {
            if (encoded[i + j] != '=') {
                num = num << 6;
                count_bits += 6;
            }

            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);
            else if (encoded[i + j] == '+')
                num = num | 62;
            else if (encoded[i + j] == '/')
                num = num | 63;
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }

        while (count_bits != 0) {
            count_bits -= 8;
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }

    decoded_string[k] = '\0';
    return decoded_string;
}

// Fungsi untuk mendekode isi file dengan algoritma ROT13
void decode_rot13(char *input) {
    int i;
    for (i = 0; i < strlen(input); i++) {
        if ((input[i] >= 'A' && input[i] <= 'Z')) {
            input[i] = 'A' + (input[i] - 'A' + 13) % 26;
        } else if (input[i] >= 'a' && input[i] <= 'z') {
            input[i] = 'a' + (input[i] - 'a' + 13) % 26;
        }
    }
}

// Fungsi untuk mendekode isi file dari representasi heksadesimal
void decode_hex(char *input) {
    int i, j;
    char hex[3];
    char decoded_char;

    for (i = 0, j = 0; i < strlen(input); i += 2, j++) {
        hex[0] = input[i];
        hex[1] = input[i + 1];
        hex[2] = '\0';

        sscanf(hex, "%x", &decoded_char);
        input[j] = decoded_char;
    }
    input[j] = '\0';
}

// Fungsi untuk decode isi file
static int decode_file_content(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    if (strstr(path, "/base64-") != NULL) {
        char *decoded_string = base64decoder(buf, size);
        strcpy(buf, decoded_string);
        free(decoded_string);
    } else if (strstr(path, "/rot13-") != NULL) {
        decode_rot13(buf);
    } else if (strstr(path, "/hex-") != NULL) {
        decode_hex(buf);
    } else if (strstr(path, "/rev-") != NULL) {
        strrev(buf);
    }
    return 0;
}

// Fungsi untuk mendapatkan informasi file atau folder
static int fill_stat(const char *path, struct stat *stbuf) {
    int res = 0;
    char *prefix_base64 = "/base64-";
    char *prefix_rot13 = "/rot13-";
    char *prefix_hex = "/hex-";
    char *prefix_rev = "/rev-";

    // Fungsi fill_stat mengisi stbuf dengan informasi yang benar
    if (strstr(path, prefix_base64) != NULL || strstr(path, prefix_rot13) != NULL || strstr(path, prefix_hex) != NULL || strstr(path, prefix_rev) != NULL) {
        stbuf->st_mode = S_IFREG | 0644;
        stbuf->st_nlink = 1;
        stbuf->st_size = 1024;
    } else {
        stbuf->st_mode = S_IFREG | 0644;
        stbuf->st_nlink = 1;
        stbuf->st_size = 1024;
    }

    return res;
}

// Implementasi FUSE: Fungsi getattr
static int my_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *fi) {
    int res = 0;
    memset(stbuf, 0, sizeof(struct stat));

    // Mendapatkan informasi file atau folder
    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        res = fill_stat(path, stbuf);
    }

    return res;
}

static struct fuse_operations fuse_oper = {
    .getattr = my_getattr,
    .read = decode_file_content,
};

int main(int argc, char *argv[]) {
    return fuse_main(argc, argv, &fuse_oper, NULL);
}
```
1. Fungsi Dekode:
   - Fungsi `strrev`: Membalikkan string yang diberikan.

   - Fungsi `base64decoder`: Mendekode string yang merupakan representasi Base64 menjadi string asli. Menggunakan teknik decoding Base64 standar.

   - Fungsi `decode_rot13`: Mendekode string dengan algoritma ROT13, yang menggeser setiap karakter alfabet 13 posisi.

   - Fungsi `decode_hex`: Mendekode string yang merupakan representasi heksadesimal ke dalam bentuk asli.

   - Fungsi `decode_file_content`: Mengecek prefix pada path file dan mendekode konten file sesuai dengan jenis prefix yang ditemukan, menggunakan fungsi-fungsi dekode di atas.

2. Fungsi Informasi File atau Folder:
   - Fungsi `fill_stat`: Mengisi struktur `struct stat` (`stbuf`) dengan informasi yang sesuai, tergantung pada jenis prefix pada path file. Misalnya, menetapkan permission, ukuran, dan jenis file.

3. Fungsi FUSE:
   - Fungsi `my_getattr`: Mengecek path file atau folder, dan jika merupakan folder root ("/"), menetapkan informasi seperti permission dan link count. Jika bukan root, memanggil fungsi `fill_stat` untuk menetapkan informasi sesuai dengan jenis prefix pada path.

4. Main:
   - `fuse_oper` struct: Membuat struktur data `fuse_operations` yang menentukan fungsi-fungsi operasi FUSE yang akan digunakan (dalam hal ini, `getattr` dan `read`).

   - Fungsi `main`: Memulai eksekusi program dengan memanggil `fuse_main` dengan argumen argc, argv, pointer ke `fuse_oper`, dan NULL. `fuse_main` digunakan untuk amenjalankan FUSE dengan operasi yang telah ditentukan.

**D.** Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus. 
- Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas). 
```c
#define PASSWORD_SECRETO "s3cr3tP@ssw0rd"

// Fungsi untuk mengecek password
int check_password(const char *password) {
    return strcmp(password, PASSWORD_SECRETO) == 0;
}

// Fungsi callback untuk membaca direktori
static int readdir_callback(const char *path, void *buf, fuse_fill_dir_t filler,
    off_t offset, struct fuse_file_info *fi) {
    (void) offset;
    (void) fi;
    if (strstr(path, "/disable-area") != NULL && !check_password(fuse_get_context()->private_data)) {
        return -EACCES;
    }
    filler(buf, ".", NULL, 0, 0);
    filler(buf, "..", NULL, 0, 0);

    return 0;
}

// Fungsi callback untuk membuka file
static int open_callback(const char *path, struct fuse_file_info *fi) {
    if (strstr(path, "/disable-area") != NULL && !check_password(fuse_get_context()->private_data)) {
        return -EACCES;
    }
    return 0;
}

// Fungsi callback untuk membaca file
static int read_callback(const char *path, char *buf, size_t size, off_t offset,
    struct fuse_file_info *fi) {
    if (strstr(path, "/disable-area") != NULL && !check_password(fuse_get_context()->private_data)) {
        return -EACCES;
    }
    return 0;
}

// Inisialisasi struktur operasi fuse
static struct fuse_operations operations = {
    .readdir = readdir_callback,
    .open = open_callback,
    .read = read_callback,
};

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <mountpoint> <password>\n", argv[0]);
        return 1;
    }

    return fuse_main(argc, argv, &operations, argv[2]);
}

```
1. Fungsi `check_password`:
   - Fungsi ini digunakan untuk memeriksa apakah password yang dimasukkan sesuai dengan password yang telah ditentukan (`PASSWORD_SECRETO`).

2. Callback FUSE:
   - Fungsi `readdir_callback`:
      - Callback untuk membaca direktori. Jika path mencakup '/disable-area' dan password belum dimasukkan, maka kembalikan error Permission denied (`-EACCES`).
      - Fungsi filler digunakan untuk menambahkan entry direktori "." dan ".." ke dalam buffer.
      - Di sini, Anda dapat menambahkan logika tambahan untuk menambahkan entry direktori lainnya sesuai kebutuhan.

   - Fungsi `open_callback`:
      - Callback untuk membuka file. Jika path mencakup '/disable-area' dan password belum dimasukkan, kembalikan error Permission denied (`-EACCES`).
      - Implementasi membuka file lainnya dapat ditambahkan sesuai kebutuhan.

   - Fungsi `read_callback`:
      - Callback untuk membaca isi file. Jika path mencakup '/disable-area' dan password belum dimasukkan, kembalikan error Permission denied (`-EACCES`).
      - Implementasi membaca file lainnya dapat ditambahkan sesuai kebutuhan.

3. Inisialisasi FUSE:
   - Fungsi `operations` struct:
      - Membuat struktur data `fuse_operations` yang menentukan fungsi-fungsi operasi FUSE yang akan digunakan (dalam hal ini, `readdir`, `open`, dan `read`).

4. Fungsi Main:
   - Memeriksa jumlah argumen saat menjalankan program. Harus ada dua argumen: `<mountpoint>` dan `<password>`.
   - Memanggil `fuse_main` untuk menjalankan FUSE dengan operasi yang telah ditentukan (`operations`). Password dimasukkan sebagai `private_data` dalam konteks FUSE.

**E.** Setiap proses yang dilakukan akan tercatat pada logs-fuse.log.
```c
// Fungsi untuk melakukan logging
static void log_operation(const char *result, const char *operation, const char *path) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    // Format waktu menjadi "dd/mm/yyyy-hh:mm:ss"
    char time_str[20];
    strftime(time_str, 20, "%d/%m/%Y-%H:%M:%S", tm_info);

    // Buka file logs-fuse.log untuk ditambahkan
    FILE *log_file = fopen("logs-fuse.log", "a");

    if (log_file != NULL) {
        fprintf(log_file, "[%s]::%s::%s::%s\n", result, time_str, operation, path);
        fclose(log_file);
    }
}
```
1. Fungsi `log_operation`:
   - Parameters:
     - `result`: Status atau hasil dari operasi (misalnya, "SUCCESS" atau "FAILURE").
     - `operation`: Nama operasi yang dicatat (misalnya, "READ" atau "WRITE").
     - `path`: Path file atau direktori yang terlibat dalam operasi.

   - Operasi:
     - Mendapatkan waktu saat ini menggunakan fungsi `time` dan mengonversinya menjadi struktur waktu lokal menggunakan `localtime`.
     - Mengformat waktu ke dalam string dengan format "dd/mm/yyyy-hh:mm:ss" menggunakan `strftime`.
     - Membuka file logs-fuse.log untuk ditambahkan (mode "a" pada `fopen`).
     - Jika file berhasil dibuka, mencetak log ke dalam file menggunakan `fprintf` dengan format "[%s]::%s::%s::%s\n", yang mencakup hasil, waktu, operasi, dan path.
     - Menutup file log setelah penulisan selesai menggunakan `fclose`.

## Output
![mountfuse](gambar/Soal1m4.png)

# Soal 2
Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari

## Pengerjaan
### Membuat file open-password.c untuk membaca file zip-pass.txt

- Melakukan proses dekripsi base64 terhadap file tersebut
- Lalu unzip home.zip menggunakan password hasil dekripsi

##### kode program
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PASSWORD_LENGTH 100

// Fungsi untuk melakukan dekripsi Base64
void base64_decode(const char* input, char* output) {
    const char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int input_length = strlen(input);
    int output_length = 0;

    while (input_length > 0) {
        char chunk[4];
        int i, j;

        // Membaca 4 karakter dari input
        for (i = 0; i < 4; i++) {
            if (input_length > 0) {
                chunk[i] = *input;
                input++;
                input_length--;
            } else {
                chunk[i] = '=';
            }
        }

        // Mendekode 4 karakter menjadi 3 byte
        unsigned char decoded_chunk[3];
        for (i = 0; i < 4; i++) {
            for (j = 0; j < 64; j++) {
                if (chunk[i] == base64_chars[j]) {
                    chunk[i] = j;
                    break;
                }
            }
        }

        decoded_chunk[0] = (chunk[0] << 2) | ((chunk[1] & 0x30) >> 4);
        decoded_chunk[1] = ((chunk[1] & 0x0F) << 4) | ((chunk[2] & 0x3C) >> 2);
        decoded_chunk[2] = ((chunk[2] & 0x03) << 6) | chunk[3];

        // Menyimpan hasil dekripsi ke output
        for (i = 0; i < 3; i++) {
            if (chunk[i + 1] == '=') {
                break;
            }
            output[output_length] = decoded_chunk[i];
            output_length++;
        }
    }

    output[output_length] = '\0';
}

int main() {
    FILE* passFile;
    char password[MAX_PASSWORD_LENGTH];
    char decodedPassword[MAX_PASSWORD_LENGTH];

    passFile = fopen("zip-pass.txt", "r");
    if (passFile == NULL) {
        printf("Gagal membuka file zip-pass.txt\n");
        return 1;
    }

    fgets(password, MAX_PASSWORD_LENGTH, passFile);

    password[strcspn(password, "\n")] = 0;

    fclose(passFile);

    base64_decode(password, decodedPassword);

    char unzipCommand[100 + MAX_PASSWORD_LENGTH + 27];
    sprintf(unzipCommand, "unzip -P %s home.zip", decodedPassword);
    system(unzipCommand);

    return 0;
}
```
##### Penjelasan Kode
- `#include <stdio.h>`: Mendeklarasikan penggunaan fungsi input/output standar dari library stdio.
- `#include <stdlib.h>`: Mendeklarasikan penggunaan fungsi umum dari library stdlib.
- `#include <string.h>`: Mendeklarasikan penggunaan fungsi-fungsi untuk manipulasi string dari library string.
- `#define MAX_PASSWORD_LENGTH 100`: Mendefinisikan konstanta `MAX_PASSWORD_LENGTH` sebagai panjang maksimal password.
- `void base64_decode(const char* input, char* output) { ... }`: Deklarasi fungsi `base64_decode` untuk melakukan dekripsi Base64.
- `int main() { ... }`: Mulai dari fungsi `main`.
- `FILE* passFile;`: Deklarasi pointer ke objek FILE yang akan digunakan untuk membaca file "zip-pass.txt".
- `char password[MAX_PASSWORD_LENGTH];`: Deklarasi array untuk menyimpan password yang terenkripsi.
- `char decodedPassword[MAX_PASSWORD_LENGTH];`: Deklarasi array untuk menyimpan password yang sudah didekripsi.
- `passFile = fopen("zip-pass.txt", "r");`: Membuka file "zip-pass.txt" untuk dibaca.
- `if (passFile == NULL) { printf("Gagal membuka file zip-pass.txt\n"); return 1; }`: Pemeriksaan apakah file berhasil dibuka. Jika tidak, program mencetak pesan kesalahan dan keluar dengan status 1.
- `fgets(password, MAX_PASSWORD_LENGTH, passFile);`: Membaca password dari file dan menyimpannya dalam array `password`.
- `password[strcspn(password, "\n")] = 0;`: Menghapus karakter newline (jika ada) dari password.
- `fclose(passFile);`: Menutup file "zip-pass.txt".
- `base64_decode(password, decodedPassword);`: Memanggil fungsi `base64_decode` untuk mendekripsi password dari format Base64.
- `char unzipCommand[100 + MAX_PASSWORD_LENGTH + 27];`: Deklarasi array untuk menyimpan string perintah unzip.
- `sprintf(unzipCommand, "unzip -P %s home.zip", decodedPassword);`: Membuat string perintah unzip dengan menggunakan password yang sudah didekripsi.
- `system(unzipCommand);`: Menjalankan perintah unzip menggunakan system call.
- `return 0;`: Keluar dari program dengan status 0 (tidak ada kesalahan).

##### Output
![openpw](gambar/2-open-password.png)

### Membuat file semangat.c

#### 1. Setiap proses yang dilakukan akan tercatat pada logs-fuse.log

##### Kode program
```
#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/xattr.h>
#include <time.h>

#define MAX_PASSWORD_LENGTH 1024

static const char *dirpath = "/home/baebluu/praktikum/sisop/fix2";

void buatLog(const char *status, const char *action, const char *info) {
    time_t t;
    struct tm *tm_now;
    time(&t);
    tm_now = localtime(&t);

    FILE *log_file = fopen("/home/baebluu/praktikum/sisop/fix2/logs-fuse.log", "a");
    if (log_file != NULL) {
        fprintf(log_file, "[%s]::%02d/%02d/%d-%02d:%02d:%02d::%s::%s\n",
                status, tm_now->tm_mday, tm_now->tm_mon + 1, tm_now->tm_year + 1900,
                tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec, action, info);
        fclose(log_file);
    }
}

static int smg_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        buatLog("FAILED", "getattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "getattr", path);
    return 0;
}

static int smg_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        buatLog("FAILED", "readdir", path);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    if (res == 0) {
        buatLog("SUCCESS", "readdir", path);
    } else {
        buatLog("FAILED", "readdir", path);
    }

    return 0;
}

static int smg_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        buatLog("FAILED", "read", path);
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        buatLog("FAILED", "read", path);
        res = -errno;
    } else {
        buatLog("SUCCESS", "read", path);
    }

    close(fd);

    return res;
}

static int smg_open(const char *path, struct fuse_file_info *fi) {
    if (strcmp(path, "/website/content.csv") == 0) {
        buatLog("SUCCESS", "open", path);
        return 0;
    }
}

static int smg_unlink(const char *path) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    res = unlink(fpath);

    if (res == -1) {
        buatLog("FAILED", "unlink", path);
        return -errno;
    }

    buatLog("SUCCESS", "unlink", path);
    return 0;
}

static struct fuse_operations smg_oper ={
    .getattr =smg_getattr,
    .readdir =smg_readdir,
    .read =smg_read,
    .open =smg_open,
    .unlink =smg_unlink,
};
```
##### Penjelasan Kode
- `#define FUSE_USE_VERSION 31`: Mendefinisikan versi FUSE yang akan digunakan.
- `#include <fuse.h>`: Mengimpor library FUSE yang diperlukan untuk mengembangkan filesystem.
- `#include <stdio.h>`, `#include <stdlib.h>`, dst.: Mengimpor library standar untuk fungsi-fungsi umum.
- `#define MAX_PASSWORD_LENGTH 1024`: Mendefinisikan panjang maksimal password.
- `static const char *dirpath = "/home/baebluu/praktikum/sisop/fix2";`: Menyimpan path dasar filesystem yang akan dioperasikan.
- `void buatLog(const char *status, const char *action, const char *info) { ... }`: Mendefinisikan fungsi `buatLog` untuk membuat log dengan waktu dan informasi tertentu. Log akan disimpan di file "logs-fuse.log".
- `static int smg_getattr(const char *path, struct stat *stbuf) { ... }`: Mendefinisikan fungsi `smg_getattr` untuk mendapatkan atribut dari suatu path. Jika berhasil, fungsi mencatat log "SUCCESS", jika tidak, mencatat log "FAILED".
- `static int smg_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) { ... }`: Mendefinisikan fungsi `smg_readdir` untuk membaca isi dari suatu direktori. Jika berhasil, mencatat log "SUCCESS", jika tidak, mencatat log "FAILED".
- `static int smg_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { ... }`: Mendefinisikan fungsi `smg_read` untuk membaca isi dari suatu file. Jika berhasil, mencatat log "SUCCESS", jika tidak, mencatat log "FAILED".
- `static int smg_open(const char *path, struct fuse_file_info *fi) { ... }`: Mendefinisikan fungsi `smg_open` untuk membuka suatu file. Jika file yang dibuka adalah "/website/content.csv", mencatat log "SUCCESS".
- `static int smg_unlink(const char *path) { ... }`: Mendefinisikan fungsi `smg_unlink` untuk menghapus suatu file. Jika berhasil, mencatat log "SUCCESS", jika tidak, mencatat log "FAILED".
- `static struct fuse_operations smg_oper ={ ... }`: Mendefinisikan struktur yang berisi fungsi-fungsi operasi filesystem. Struktur ini akan digunakan oleh FUSE untuk mengarahkan operasi-operasi filesystem ke fungsi-fungsi yang telah didefinisikan.

##### Output
![logs](gambar/2-logs.png)

#### 2. Mengklasifikasikan file sesuai jenisnya

Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan:
- documents: .pdf dan .docx. 
- images: .jpg, .png, dan .ico. 
- website: .js, .html, dan .json. 
- sisop: .c dan .sh. 
- text: .txt. 
- aI: .ipynb dan .csv.

##### kode program
```
void pindahFile(const char *sourcePath, const char *destinationPath) {
    struct stat st = {0};

    char destinationDir[512];
    strncpy(destinationDir, destinationPath, sizeof(destinationDir));
    char *lastSlash = strrchr(destinationDir, '/');
    if (lastSlash != NULL) {
        *lastSlash = '\0';
        if (stat(destinationDir, &st) == -1) {
            if (mkdir(destinationDir, 0770) != 0) {
                perror("Error creating destination directory");
                return;
            }
        }
    }

    if (rename(sourcePath, destinationPath) != 0) {
        perror("Error moving file");
        buatLog("FAILED", "pindahFile", sourcePath);
    } else {
        buatLog("SUCCESS", "pindahFile", sourcePath);
    }
}

void buatDirectory(const char *path) {
    struct stat st = {0};
    if (stat(path, &st) == -1) {
        if (mkdir(path, 0770) != 0) {
            perror("Error creating directory");
        } else {
            buatLog("SUCCESS", "Create Directory", path);
        }
    } else {
        buatLog("FAILED", "Create Directory", "Directory already exists");
    }
}

void kategoriFile(const char *fileName) {
    char filepath[512];
    snprintf(filepath, sizeof(filepath), "%s", fileName);

    const char *dot = strrchr(fileName, '.');
    const char *extension = dot ? (dot + 1) : "unknown";

    const char *category;
    if (strcmp(extension, "pdf") == 0 || strcmp(extension, "docx") == 0) {
        category = "documents";
    } else if (strcmp(extension, "jpg") == 0 || strcmp(extension, "png") == 0 || strcmp(extension, "ico") == 0) {
        category = "images";
    } else if (strcmp(extension, "js") == 0 || strcmp(extension, "html") == 0 || strcmp(extension, "json") == 0) {
        category = "website";
    } else if (strcmp(extension, "c") == 0 || strcmp(extension, "sh") == 0) {
        category = "sisop";
    } else if (strcmp(extension, "txt") == 0) {
        category = "text";
    } else if (strcmp(extension, "ipynb") == 0 || strcmp(extension, "csv") == 0) {
        category = "aI";
    } else {
        return;
    }

    char category_folder[32];
    snprintf(category_folder, sizeof(category_folder), "%s", category);

    buatDirectory(category_folder);

    char new_filepath[512];
    snprintf(new_filepath, sizeof(new_filepath), "%s/%s", category_folder, fileName);

    pindahFile(filepath, new_filepath);
}

void FilesByCategory(){
    struct dirent *dir;
    DIR *d;
    d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
            {
                kategoriFile(dir->d_name);
            }
        }
        closedir(d);
    }
}
```
##### Penjelasan kode

###### Fungsi pindahFile
- `struct stat st = {0};`: Membuat variabel `st` dari tipe `struct stat` dan menginisialisasinya dengan nol.
- `char destinationDir[512];`: Membuat array `destinationDir` untuk menyimpan path direktori tujuan.
- `strncpy(destinationDir, destinationPath, sizeof(destinationDir));`: Meng-copy `destinationPath` ke dalam `destinationDir`.
- `char *lastSlash = strrchr(destinationDir, '/');`: Mencari karakter '/' terakhir dalam `destinationDir`.
- `if (lastSlash != NULL) { ... }`: Memeriksa apakah '/' ditemukan.
- `*lastSlash = '\0';`: Mengakhiri string pada karakter '/' dengan null terminator, sehingga hanya menyisakan path direktori.
- `if (stat(destinationDir, &st) == -1) { ... }`: Memeriksa apakah direktori tujuan sudah ada. Jika tidak, membuat direktori baru.
- `if (rename(sourcePath, destinationPath) != 0) { ... }`: Memindahkan file dari `sourcePath` ke `destinationPath`. Jika gagal, mencetak pesan kesalahan.
- `buatform("FAILED", "pindahFile", sourcePath);`: Mencatat ke log bahwa operasi `pindahFile` gagal.
- `buatform("SUCCESS", "pindahFile", sourcePath);`: Mencatat ke log bahwa operasi `pindahFile` berhasil.

###### Fungsi buatDirectory

- `struct stat st = {0};`: Membuat variabel `st` dari tipe `struct stat` dan menginisialisasinya dengan nol.
- `if (stat(path, &st) == -1) { ... }`: Memeriksa apakah direktori dengan `path` sudah ada.
- `if (mkdir(path, 0770) != 0) { ... }`: Jika belum ada, membuat direktori baru dengan izin 0770 (rwxrwx---).
- `perror("Error creating directory");`: Mencetak pesan kesalahan jika pembuatan direktori gagal.
- `buatLog("SUCCESS", "Create Directory", path);`: Mencatat ke log bahwa pembuatan direktori berhasil.
- `buatLog("FAILED", "Create Directory", "Directory already exists");`: Mencatat ke log bahwa direktori sudah ada.

###### Fungsi kategoriFile
- `char filepath[512]; snprintf(filepath, sizeof(filepath), "%s", fileName);`: Membuat salinan path file ke `filepath`.
- `const char *dot = strrchr(fileName, '.'); const char *extension = dot ? (dot + 1) : "unknown";`: Menentukan ekstensi file.
- `const char *category; ... if (strcmp(extension, "ipynb") == 0 || strcmp(extension, "csv") == 0) { category = "aI"; } else { return; }`: Menentukan kategori file berdasarkan ekstensinya. Jika tidak sesuai, keluar dari fungsi.
- `char category_folder[32]; snprintf(category_folder, sizeof(category_folder), "%s", category);`: Membuat string dengan nama folder sesuai kategori.
- `buatDirectory(category_folder);`: Membuat direktori sesuai kategori jika belum ada.
- `char new_filepath[512]; snprintf(new_filepath, sizeof(new_filepath), "%s/%s", category_folder, fileName);`: Membuat path baru untuk file dalam kategori tersebut.
- `pindahFile(filepath, new_filepath);`: Memindahkan file ke folder yang sesuai.

###### Fungsi FilesByCategory
- `struct dirent *dir; DIR *d; d = opendir(".");`: Membuka direktori saat ini untuk membaca file-file di dalamnya.
- `while ((dir = readdir(d)) != NULL) { ... }`: Membaca setiap file di dalam direktori saat ini.
- `if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) { kategoriFile(dir->d_name); }`: Memanggil fungsi `kategoriFile` untuk setiap file regular (bukan direktori) yang bukan "." atau "..".
- `closedir(d);`: Menutup direktori setelah selesai membaca file.

##### Output
![kategori](gambar/2-kategori.png)

#### 3. Setiap mengkases file di dalam folder text maka perlu memasukkan password terlebih dahulu
Password terdapat di file password.bin

##### Kode Program
```
void checkPassword(const char *password) {
    FILE *passwordFile = fopen("/home/baebluu/praktikum/sisop/fix2/password.bin", "r");
    if (passwordFile == NULL) {
        perror("Error opening password file");
        return; // or take appropriate action
    }

    char storedPassword[MAX_PASSWORD_LENGTH];
    fgets(storedPassword, sizeof(storedPassword), passwordFile);
    fclose(passwordFile);

    // Remove newline character from the password read from the file
    char *newline = strchr(storedPassword, '\n');
    if (newline != NULL) {
        *newline = '\0';
    }

    if (strcmp(password, storedPassword) != 0) {
        printf("Incorrect password\n");
        return -EACCES;
        // or take appropriate action, e.g., deny access or exit the program
    }
}
```
##### Penjelasan Kode
1. `FILE *passwordFile = fopen("/home/baebluu/praktikum/sisop/fix2/password.bin", "r");`: Membuka file password (`password.bin`) dalam mode baca ("r").
2. `if (passwordFile == NULL) { perror("Error opening password file"); return; }`: Memeriksa apakah pembukaan file berhasil. Jika tidak, mencetak pesan kesalahan dan keluar dari fungsi.
3. `char storedPassword[MAX_PASSWORD_LENGTH]; fgets(storedPassword, sizeof(storedPassword), passwordFile);`: Membaca password yang tersimpan di file dan menyimpannya dalam array `storedPassword`.
4. `fclose(passwordFile);`: Menutup file setelah selesai membacanya.
5. `char *newline = strchr(storedPassword, '\n'); if (newline != NULL) { *newline = '\0'; }`: Menghapus karakter newline ('\n') dari password yang dibaca. Ini membantu membandingkan password dengan lebih akurat.
6. `if (strcmp(password, storedPassword) != 0) { printf("Incorrect password\n"); return -EACCES; }`: Membandingkan password yang dimasukkan dengan password yang tersimpan. Jika tidak cocok, mencetak pesan kesalahan dan keluar dari fungsi dengan mengembalikan kode kesalahan (-EACCES). Pilihan ini dapat digunakan untuk menolak akses atau keluar dari program.
7. **Catatan**: Penggunaan `return -EACCES;` dalam fungsi yang seharusnya mengembalikan `void` tidak sesuai dengan deklarasi fungsi. Biasanya, Anda dapat memilih untuk mengganti `void` menjadi `int` dan mengembalikan nilai status atau menggunakan `exit(code)` untuk keluar dari program dengan kode keluar tertentu.


**Memodifikasi fungsi smg_read**
```
static int smg_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    // Check if the file is in the "text" folder
    if (strstr(path, "/text/") != NULL) {
        // Ask for password
        char password[MAX_PASSWORD_LENGTH];
        printf("Enter password: ");
        if (scanf("%s", password) != 1) {
            printf("Error reading password\n");
            return -EACCES; // or appropriate error code
        }

        // Check the password
        checkPassword(password);
        // If the password is incorrect, access will be denied in checkPassword

    }

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        buatLog("FAILED", "read", path);
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        buatLog("FAILED", "read", path);
        res = -errno;
    } else {
        buatLog("SUCCESS", "read", path);
    }

    close(fd);

    return res;
}
```
##### Penjelasan Kode
1. `char fpath[1000];`: Membuat array karakter `fpath` untuk menyimpan path file setelah diperoleh.
2. `if (strcmp(path, "/") == 0) { path = dirpath; sprintf(fpath, "%s", path); } else sprintf(fpath, "%s%s", dirpath, path);`: Mengonstruksi path file (`fpath`) berdasarkan kondisi apakah `path` adalah "/" atau bukan.
3. `int res = 0; int fd = 0;`: Mendeklarasikan variabel `res` untuk menyimpan nilai kembalian, dan `fd` sebagai file descriptor.
4. `(void)fi;`: Menggunakan `(void)` untuk menonaktifkan peringatan terkait variabel yang tidak digunakan.
5. `if (strstr(path, "/text/") != NULL) { ... }`: Memeriksa apakah file berada dalam folder "/text/". Jika ya, meminta pengguna memasukkan password dan memanggil fungsi `checkPassword`.
6. `fd = open(fpath, O_RDONLY);`: Membuka file dengan hak akses hanya baca (`O_RDONLY`) dan mendapatkan file descriptor (`fd`).
7. `if (fd == -1) { buatLog("FAILED", "read", path); return -errno; }`: Memeriksa apakah pembukaan file berhasil. Jika tidak, mencetak pesan kesalahan dan mengembalikan kode kesalahan.
8. `res = pread(fd, buf, size, offset);`: Membaca data dari file menggunakan `pread` ke buffer `buf` dengan ukuran `size` dan offset tertentu.
9. `if (res == -1) { buatLog("FAILED", "read", path); res = -errno; } else { buatLog("SUCCESS", "read", path); }`: Memeriksa apakah operasi pembacaan berhasil atau gagal. Mencetak log sesuai dengan hasilnya.
10. `close(fd);`: Menutup file setelah selesai membacanya.
11. `return res;`: Mengembalikan nilai res, yang mungkin berisi data yang dibaca atau kode kesalahan jika ada.


#### 4. Pada folder website
- Membuat file csv pada folder website dengan format ini: file,title,body
- Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv

##### Kode Program
```
void generateHtmlFromCsv(const char *csvPath) {
    FILE *csvFile = fopen(csvPath, "r");
    if (csvFile == NULL) {
        perror("Error opening CSV file");
        return;
    }

    char line[1024];
    while (fgets(line, sizeof(line), csvFile) != NULL) {
        char filename[256], title[256], body[512];
        if (sscanf(line, "%[^,],%[^,],%[^\n]", filename, title, body) == 3) {
            // Generate HTML content
            char htmlContent[1024];
            snprintf(htmlContent, sizeof(htmlContent), "<html><head><title>%s</title></head><body>%s</body></html>", title, body);

            // Create HTML file
            char htmlFilename[266]; // (257 + 9 = 266)
            snprintf(htmlFilename, sizeof(htmlFilename), "website/%s", filename);

            FILE *htmlFile = fopen(htmlFilename, "w");
            if (htmlFile != NULL) {
                fprintf(htmlFile, "%s", htmlContent);
                fclose(htmlFile);
            } else {
                perror("Error creating HTML file");
            }
        }
    }

    fclose(csvFile);
}

void ContentCSV() {
    FILE *csv_file = fopen("website/content.csv", "w");
    if (csv_file == NULL) {
        perror("Error creating content.csv");
        return;
    }

    fprintf(csv_file, "file,title,body\n");

    DIR *d;
    struct dirent *dir;
    d = opendir("website");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                char html_path[512];
                snprintf(html_path, sizeof(html_path), "website/%s", dir->d_name);

                FILE *html_file = fopen(html_path, "r");
                if (html_file != NULL) {
                    char line[1024];
                    char title[1024] = "";
                    char body[1024] = "";
                    char filename[1024] = "";
                    strncpy(filename, dir->d_name, sizeof(filename));

                    while (fgets(line, sizeof(line), html_file) != NULL) {
                        char *title_start = strstr(line, "<title>");
                        char *body_start = strstr(line, " <body>");

                        if (title_start != NULL) {
                            strcpy(title, title_start + 7);
                            char *title_end = strstr(title, "</title>");
                            if (title_end != NULL) {
                                *title_end = '\0';
                                char *html_extension = strstr(title, ".html");
                                if (html_extension != NULL) {
                                    *html_extension = '\0';
                                }
                            }
                        }

                        if (body_start != NULL) {
                            strcpy(body, body_start + 6);
                            char *body_end = strstr(body, " </body>");
                            if (body_end != NULL) {
                                *body_end = '\0';
                            }
                        }
                    }

                    fprintf(csv_file, "%s,%s,%s\n", filename, title, body);

                    fclose(html_file);
                }
            }
        }

        closedir(d);
        fclose(csv_file);
    }

    // Generate HTML files from CSV
    generateHtmlFromCsv("website/content.csv");
}
```
##### Penjelasan Kode
1. `FILE *csvFile = fopen(csvPath, "r");`: Membuka file CSV untuk dibaca.
2. `if (csvFile == NULL) { perror("Error opening CSV file"); return; }`: Memeriksa apakah file CSV berhasil dibuka. Jika tidak, mencetak pesan kesalahan dan menghentikan eksekusi.
3. `while (fgets(line, sizeof(line), csvFile) != NULL) { ... }`: Membaca setiap baris dari file CSV dan menyimpannya dalam array `line`.
4. `char filename[256], title[256], body[512];`: Mendeklarasikan variabel untuk menyimpan filename, title, dan body dari setiap baris.
5. `if (sscanf(line, "%[^,],%[^,],%[^\n]", filename, title, body) == 3) { ... }`: Menggunakan `sscanf` untuk membaca data dari baris CSV dan menyimpannya dalam variabel yang sesuai. Hanya melanjutkan jika pembacaan berhasil.
6. `char htmlContent[1024]; snprintf(htmlContent, sizeof(htmlContent), "<html><head><title>%s</title></head><body>%s</body></html>", title, body);`: Membuat konten HTML dengan menggunakan data title dan body.
7. `char htmlFilename[266]; snprintf(htmlFilename, sizeof(htmlFilename), "website/%s", filename);`: Membuat nama file HTML berdasarkan filename.
8. `FILE *htmlFile = fopen(htmlFilename, "w");`: Membuka file HTML untuk ditulis.
9. `if (htmlFile != NULL) { fprintf(htmlFile, "%s", htmlContent); fclose(htmlFile); } else { perror("Error creating HTML file"); }`: Menulis konten HTML ke file HTML yang baru dibuat. Jika gagal, mencetak pesan kesalahan.

**Fungsi ContentCSV**
1. `FILE *csv_file = fopen("website/content.csv", "w");`: Membuka file `content.csv` untuk ditulis.
2. `if (csv_file == NULL) { perror("Error creating content.csv"); return; }`: Memeriksa apakah file `content.csv` berhasil dibuka. Jika tidak, mencetak pesan kesalahan dan menghentikan eksekusi.
3. `fprintf(csv_file, "file,title,body\n");`: Menulis header ke file CSV.
4. `DIR *d; struct dirent *dir; d = opendir("website");`: Membuka direktori "website" untuk membaca file-file HTML.
5. `while ((dir = readdir(d)) != NULL) { ... }`: Membaca setiap file dalam direktori "website".
6. `if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) { ... }`: Memeriksa apakah entri direktori adalah file regular (bukan direktori "." atau "..").
7. `char html_path[512]; snprintf(html_path, sizeof(html_path), "website/%s", dir->d_name);`: Membuat path lengkap ke file HTML.
8. `FILE *html_file = fopen(html_path, "r");`: Membuka file HTML untuk dibaca.
9. `if (html_file != NULL) { ... }`: Memeriksa apakah file HTML berhasil dibuka.
10. `fprintf(csv_file, "%s,%s,%s\n", filename, title, body);`: Menulis informasi file HTML ke file CSV.
11. `fclose(html_file);`: Menutup file HTML setelah selesai membacanya.
12. `closedir(d); fclose(csv_file);`: Menutup direktori "website" dan file CSV setelah selesai memproses semua file.
13. `generateHtmlFromCsv("website/content.csv");`: Memanggil fungsi `generateHtmlFromCsv` untuk membuat file HTML dari data CSV yang telah dihasilkan.

##### Output
![website](gambar/2-website.png)

#### 5. Tidak dapat menghapus file / folder yang mengandung prefix “restricted”

##### Kode Program
```
static int smg_unlink(const char *path) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the file or folder has the "restricted" prefix
    if (strncmp(path, "/restricted", 11) == 0) {
        printf("Deletion of file/folder with 'restricted' prefix is not allowed.\n");
        return -EACCES; // or appropriate error code
    }

    res = unlink(fpath);

    if (res == -1) {
        buatLog("FAILED", "unlink", path);
        return -errno;
    }

    buatLog("SUCCESS", "unlink", path);
    return 0;
}
```
##### Penjelasan Kode
1. `int res;`: Mendeklarasikan variabel `res` untuk menyimpan nilai kembalian dari fungsi-fungsi yang dipanggil.
2. `char fpath[1000]; sprintf(fpath, "%s%s", dirpath, path);`: Membuat path lengkap (`fpath`) dengan menggabungkan direktori utama (`dirpath`) dan path yang diberikan (`path`).
3. `if (strncmp(path, "/restricted", 11) == 0) { printf("Deletion of file/folder with 'restricted' prefix is not allowed.\n"); return -EACCES; }`: Memeriksa apakah file atau folder memiliki awalan "/restricted". Jika ya, mencetak pesan kesalahan dan mengembalikan nilai `-EACCES` (kode akses ditolak).
4. `res = unlink(fpath);`: Menghapus file atau folder menggunakan fungsi `unlink` dengan menggunakan path lengkap.
5. `if (res == -1) { buatLog("FAILED", "unlink", path); return -errno; }`: Memeriksa apakah penghapusan berhasil. Jika tidak, mencetak pesan kesalahan dan mengembalikan nilai negatif dari variabel `errno`.
6. `buatLog("SUCCESS", "unlink", path);`: Jika penghapusan berhasil, mencatat ke log bahwa penghapusan sukses.
7. `return 0;`: Mengembalikan nilai 0 untuk menunjukkan bahwa penghapusan berhasil.


#### 6. Pada folder documents
Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut

##### Kode Program
```
static int smg_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = setxattr(fpath, name, value, size, flags);

    if (res == -1) {
        buatLog("FAILED", "setxattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "setxattr", path);
    return 0;
}


static int smg_getxattr(const char *path, const char *name, char *value, size_t size) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = getxattr(fpath, name, value, size);

    if (res == -1) {
        buatLog("FAILED", "getxattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "getxattr", path);
    return res;
}
```

##### Penjelasan Kode
1. `char fpath[1000]; sprintf(fpath, "%s%s", dirpath, path);`: Membuat path lengkap (`fpath`) dengan menggabungkan direktori utama (`dirpath`) dan path yang diberikan (`path`).
2. Pada `smg_setxattr`:
   - `int res = setxattr(fpath, name, value, size, flags);`: Mengatur atribut ekstended menggunakan fungsi `setxattr`. Variabel `res` menyimpan nilai kembalian dari fungsi tersebut.
   - `if (res == -1) { buatLog("FAILED", "setxattr", path); return -errno; }`: Memeriksa apakah setxattr berhasil. Jika tidak, mencetak pesan kesalahan dan mengembalikan nilai negatif dari variabel `errno`.
   - `buatLog("SUCCESS", "setxattr", path); return 0;`: Jika setxattr berhasil, mencatat ke log bahwa operasi sukses dan mengembalikan nilai 0.

3. Pada `smg_getxattr`:
   - `int res = getxattr(fpath, name, value, size);`: Mendapatkan nilai atribut ekstended menggunakan fungsi `getxattr`. Variabel `res` menyimpan nilai kembalian dari fungsi tersebut.
   - `if (res == -1) { buatLog("FAILED", "getxattr", path); return -errno; }`: Memeriksa apakah getxattr berhasil. Jika tidak, mencetak pesan kesalahan dan mengembalikan nilai negatif dari variabel `errno`.
   - `buatLog("SUCCESS", "getxattr", path); return res;`: Jika getxattr berhasil, mencatat ke log bahwa operasi sukses dan mengembalikan nilai yang diperoleh dari getxattr.

##### Output
![dosuments](gambar/2-documents.png)

### Kode full semangat.c 
```
#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/xattr.h>
#include <time.h>

#define MAX_PASSWORD_LENGTH 1024

static const char *dirpath = "/home/baebluu/praktikum/sisop/fix2";

void buatLog(const char *status, const char *action, const char *info) {
    time_t t;
    struct tm *tm_now;
    time(&t);
    tm_now = localtime(&t);

    FILE *log_file = fopen("/home/baebluu/praktikum/sisop/fix2/logs-fuse.log", "a");
    if (log_file != NULL) {
        fprintf(log_file, "[%s]::%02d/%02d/%d-%02d:%02d:%02d::%s::%s\n",
                status, tm_now->tm_mday, tm_now->tm_mon + 1, tm_now->tm_year + 1900,
                tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec, action, info);
        fclose(log_file);
    }
}

void pindahFile(const char *sourcePath, const char *destinationPath) {
    struct stat st = {0};

    char destinationDir[512];
    strncpy(destinationDir, destinationPath, sizeof(destinationDir));
    char *lastSlash = strrchr(destinationDir, '/');
    if (lastSlash != NULL) {
        *lastSlash = '\0';
        if (stat(destinationDir, &st) == -1) {
            if (mkdir(destinationDir, 0770) != 0) {
                perror("Error creating destination directory");
                return;
            }
        }
    }

    if (rename(sourcePath, destinationPath) != 0) {
        perror("Error moving file");
        buatLog("FAILED", "pindahFile", sourcePath);
    } else {
        buatLog("SUCCESS", "pindahFile", sourcePath);
    }
}

void buatDirectory(const char *path) {
    struct stat st = {0};
    if (stat(path, &st) == -1) {
        if (mkdir(path, 0770) != 0) {
            perror("Error creating directory");
        } else {
            buatLog("SUCCESS", "Create Directory", path);
        }
    } else {
        buatLog("FAILED", "Create Directory", "Directory already exists");
    }
}

void kategoriFile(const char *fileName) {
    char filepath[512];
    snprintf(filepath, sizeof(filepath), "%s", fileName);

    const char *dot = strrchr(fileName, '.');
    const char *extension = dot ? (dot + 1) : "unknown";

    const char *category;
    if (strcmp(extension, "pdf") == 0 || strcmp(extension, "docx") == 0) {
        category = "documents";
    } else if (strcmp(extension, "jpg") == 0 || strcmp(extension, "png") == 0 || strcmp(extension, "ico") == 0) {
        category = "images";
    } else if (strcmp(extension, "js") == 0 || strcmp(extension, "html") == 0 || strcmp(extension, "json") == 0) {
        category = "website";
    } else if (strcmp(extension, "c") == 0 || strcmp(extension, "sh") == 0) {
        category = "sisop";
    } else if (strcmp(extension, "txt") == 0) {
        category = "text";
    } else if (strcmp(extension, "ipynb") == 0 || strcmp(extension, "csv") == 0) {
        category = "aI";
    } else {
        return;
    }

    char category_folder[32];
    snprintf(category_folder, sizeof(category_folder), "%s", category);

    buatDirectory(category_folder);

    char new_filepath[512];
    snprintf(new_filepath, sizeof(new_filepath), "%s/%s", category_folder, fileName);

    pindahFile(filepath, new_filepath);
}

void FilesByCategory(){
    struct dirent *dir;
    DIR *d;
    d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
            {
                kategoriFile(dir->d_name);
            }
        }
        closedir(d);
    }
}

void checkPassword(const char *password) {
    FILE *passwordFile = fopen("/home/baebluu/praktikum/sisop/fix2/password.bin", "r");
    if (passwordFile == NULL) {
        perror("Error opening password file");
        return; // or take appropriate action
    }

    char storedPassword[MAX_PASSWORD_LENGTH];
    fgets(storedPassword, sizeof(storedPassword), passwordFile);
    fclose(passwordFile);

    // Remove newline character from the password read from the file
    char *newline = strchr(storedPassword, '\n');
    if (newline != NULL) {
        *newline = '\0';
    }

    if (strcmp(password, storedPassword) != 0) {
        printf("Incorrect password\n");
        return -EACCES;
        // or take appropriate action, e.g., deny access or exit the program
    }
}

void generateHtmlFromCsv(const char *csvPath) {
    FILE *csvFile = fopen(csvPath, "r");
    if (csvFile == NULL) {
        perror("Error opening CSV file");
        return;
    }

    char line[1024];
    while (fgets(line, sizeof(line), csvFile) != NULL) {
        char filename[256], title[256], body[512];
        if (sscanf(line, "%[^,],%[^,],%[^\n]", filename, title, body) == 3) {
            // Generate HTML content
            char htmlContent[1024];
            snprintf(htmlContent, sizeof(htmlContent), "<html><head><title>%s</title></head><body>%s</body></html>", title, body);

            // Create HTML file
            char htmlFilename[266]; // (257 + 9 = 266)
            snprintf(htmlFilename, sizeof(htmlFilename), "website/%s", filename);

            FILE *htmlFile = fopen(htmlFilename, "w");
            if (htmlFile != NULL) {
                fprintf(htmlFile, "%s", htmlContent);
                fclose(htmlFile);
            } else {
                perror("Error creating HTML file");
            }
        }
    }

    fclose(csvFile);
}

void ContentCSV() {
    FILE *csv_file = fopen("website/content.csv", "w");
    if (csv_file == NULL) {
        perror("Error creating content.csv");
        return;
    }

    fprintf(csv_file, "file,title,body\n");

    DIR *d;
    struct dirent *dir;
    d = opendir("website");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                char html_path[512];
                snprintf(html_path, sizeof(html_path), "website/%s", dir->d_name);

                FILE *html_file = fopen(html_path, "r");
                if (html_file != NULL) {
                    char line[1024];
                    char title[1024] = "";
                    char body[1024] = "";
                    char filename[1024] = "";
                    strncpy(filename, dir->d_name, sizeof(filename));

                    while (fgets(line, sizeof(line), html_file) != NULL) {
                        char *title_start = strstr(line, "<title>");
                        char *body_start = strstr(line, " <body>");

                        if (title_start != NULL) {
                            strcpy(title, title_start + 7);
                            char *title_end = strstr(title, "</title>");
                            if (title_end != NULL) {
                                *title_end = '\0';
                                char *html_extension = strstr(title, ".html");
                                if (html_extension != NULL) {
                                    *html_extension = '\0';
                                }
                            }
                        }

                        if (body_start != NULL) {
                            strcpy(body, body_start + 6);
                            char *body_end = strstr(body, " </body>");
                            if (body_end != NULL) {
                                *body_end = '\0';
                            }
                        }
                    }

                    fprintf(csv_file, "%s,%s,%s\n", filename, title, body);

                    fclose(html_file);
                }
            }
        }

        closedir(d);
        fclose(csv_file);
    }

    // Generate HTML files from CSV
    generateHtmlFromCsv("website/content.csv");
}


static int smg_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        buatLog("FAILED", "getattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "getattr", path);
    return 0;
}

static int smg_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        buatLog("FAILED", "readdir", path);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    if (res == 0) {
        buatLog("SUCCESS", "readdir", path);
    } else {
        buatLog("FAILED", "readdir", path);
    }

    return 0;
}

static int smg_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    // Check if the file is in the "text" folder
    if (strstr(path, "/text/") != NULL) {
        // Ask for password
        char password[MAX_PASSWORD_LENGTH];
        printf("Enter password: ");
        if (scanf("%s", password) != 1) {
            printf("Error reading password\n");
            return -EACCES; // or appropriate error code
        }

        // Check the password
        checkPassword(password);
        // If the password is incorrect, access will be denied in checkPassword

    }

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        buatLog("FAILED", "read", path);
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        buatLog("FAILED", "read", path);
        res = -errno;
    } else {
        buatLog("SUCCESS", "read", path);
    }

    close(fd);

    return res;
}

static int smg_open(const char *path, struct fuse_file_info *fi) {
    if (strcmp(path, "/website/content.csv") == 0) {
        buatLog("SUCCESS", "open", path);
        return 0;
    } else {
        buatLog("OPEN", "open", path);
        return 0;  // or appropriate error code
    }
}

static int smg_unlink(const char *path) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the file or folder has the "restricted" prefix
    if (strncmp(path, "/restricted", 11) == 0) {
        printf("Deletion of file/folder with 'restricted' prefix is not allowed.\n");
        return -EACCES; // or appropriate error code
    }

    res = unlink(fpath);

    if (res == -1) {
        buatLog("FAILED", "unlink", path);
        return -errno;
    }

    buatLog("SUCCESS", "unlink", path);
    return 0;
}


static int smg_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = setxattr(fpath, name, value, size, flags);

    if (res == -1) {
        buatLog("FAILED", "setxattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "setxattr", path);
    return 0;
}


static int smg_getxattr(const char *path, const char *name, char *value, size_t size) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = getxattr(fpath, name, value, size);

    if (res == -1) {
        buatLog("FAILED", "getxattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "getxattr", path);
    return res;
}


static struct fuse_operations smg_oper = {
    .getattr = smg_getattr,
    .readdir = smg_readdir,
    .read = smg_read,
    .open = smg_open,
    .unlink = smg_unlink,
    .setxattr = smg_setxattr,
    .getxattr = smg_getxattr,
};


int main(int argc, char *argv[]) {

    umask(0);

    FilesByCategory();
    ContentCSV();

    return fuse_main(argc, argv, &smg_oper, NULL);
}

```

### Membuat file server.c
Manda diminta untuk membuat sebuah server (socket programming) untuk membaca webtoon.csv. Dimana terjadi pengiriman data antara client ke server dan server ke client.
- Menampilkan seluruh judul
- Menampilkan berdasarkan genre
- Menampilkan berdasarkan hari
- Menambahkan ke dalam file webtoon.csv
- Melakukan delete berdasarkan judul
- Selain command yang diberikan akan menampilkan tulisan “Invalid Command”

Manfaatkan client.c pada folder sisop sebagai client

##### Kode Program
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#define PORT 8080
#define MAX_BUFFER_SIZE 1024

void sendToClient(int new_socket, const char *message) {
    send(new_socket, message, strlen(message), 0);
}

void readWebtoonData(FILE *webtoonFile, char *result) {
    char line[MAX_BUFFER_SIZE];
    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        strcat(result, line);
    }
}

void showAllTitles(int new_socket) {
    FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
    if (webtoonFile == NULL) {
        sendToClient(new_socket, "Error opening webtoon.csv\n");
        return;
    }

    char result[MAX_BUFFER_SIZE] = "All Titles:\n";
    readWebtoonData(webtoonFile, result);
    sendToClient(new_socket, result);

    fclose(webtoonFile);
}

void showByGenre(int new_socket, const char *genre) {
    FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
    if (webtoonFile == NULL) {
        sendToClient(new_socket, "Error opening webtoon.csv\n");
        return;
    }

    char result[MAX_BUFFER_SIZE] = "";
    char line[MAX_BUFFER_SIZE];

    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        char temp[MAX_BUFFER_SIZE];
        snprintf(temp, sizeof(temp), "%s", line);

        char *tok = strtok(temp, ",");
        if (tok != NULL && strcmp(tok, genre) == 0) {
            strcat(result, line);
        }
    }

    if (strlen(result) == 0) {
        sendToClient(new_socket, "No titles found for the given genre.\n");
    } else {
        sendToClient(new_socket, result);
    }

    fclose(webtoonFile);
}

void showByDay(int new_socket, const char *day) {
    FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
    if (webtoonFile == NULL) {
        sendToClient(new_socket, "Error opening webtoon.csv\n");
        return;
    }

    char result[MAX_BUFFER_SIZE] = "";
    char line[MAX_BUFFER_SIZE];

    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        char temp[MAX_BUFFER_SIZE];
        snprintf(temp, sizeof(temp), "%s", line);

        char *tok = strtok(temp, ",");
        if (tok != NULL && strcmp(tok, day) == 0) {
            strcat(result, line);
        }
    }

    if (strlen(result) == 0) {
        sendToClient(new_socket, "No titles found for the given day.\n");
    } else {
        sendToClient(new_socket, result);
    }

    fclose(webtoonFile);
}

void addToWebtoon(FILE *webtoonFile, const char *data) {
    fseek(webtoonFile, 0, SEEK_END);
    fprintf(webtoonFile, "%s", data);
}

void deleteByTitle(FILE *webtoonFile, const char *title) {
    FILE *tempFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon_temp.csv", "w");
    if (tempFile == NULL) {
        perror("Error creating temporary file");
        return;
    }

    char line[MAX_BUFFER_SIZE];
    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        char temp[MAX_BUFFER_SIZE];
        snprintf(temp, sizeof(temp), "%s", line);

        char *tok = strtok(temp, ",");
        if (tok != NULL && strcmp(tok, title) != 0) {
            fprintf(tempFile, "%s", line);
        }
    }

    fclose(tempFile);

    // Remove the original file
    remove("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv");

    // Rename the temporary file to the original file name
    rename("/home/baebluu/praktikum/sisop/fix2/aI/webtoon_temp.csv",
           "/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv");
}

// Implementasi lainnya (add, delete, invalid command) disesuaikan dengan kebutuhan

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    // Membuat file descriptor soket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Menetapkan opsi soket
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Mengikat soket ke alamat dan port tertentu
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Menunggu koneksi dari klien
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (1) {
        // Menerima koneksi baru
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        char buffer[MAX_BUFFER_SIZE] = {0};
        read(new_socket, buffer, sizeof(buffer));
        printf("Client: %s\n", buffer);

        // Membaca dan memproses perintah dari client
        if (strcmp(buffer, "show\n") == 0) {
            showAllTitles(new_socket);
        } else if (strncmp(buffer, "genre", 13) == 0) {
            char *genre = strtok(buffer + 13, "\n");
            showByGenre(new_socket, genre);
        } else if (strncmp(buffer, "hari", 12) == 0) {
            char *day = strtok(buffer + 12, "\n");
            showByDay(new_socket, day);
        } else if (strncmp(buffer, "add", 3) == 0) {
            char *data = strtok(buffer + 4, "\n");
            FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "a");
            if (webtoonFile != NULL) {
                addToWebtoon(webtoonFile, data);
                fclose(webtoonFile);
                sendToClient(new_socket, "Data added successfully.\n");
            } else {
                perror("Error opening webtoon.csv\n");
                sendToClient(new_socket, "Error adding data.\n");
            }
        } else if (strncmp(buffer, "delete", 15) == 0) {
            char *title = strtok(buffer + 16, "\n");
            FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
            if (webtoonFile != NULL) {
                deleteByTitle(webtoonFile, title);
                fclose(webtoonFile);
                sendToClient(new_socket, "Data deleted successfully.\n");
            } else {
                perror("Error opening webtoon.csv\n");
                sendToClient(new_socket, "Error deleting data.\n");
            }
        } else {
            sendToClient(new_socket, "Invalid Command\n");
        }

        // Menutup soket setelah selesai
        close(new_socket);
    }

    return 0;
}

```
##### Penjelasan Kode
Pada intinya, program ini melakukan beberapa hal utama:

1. **Membuat dan Mengonfigurasi Socket Server**: Program membuka sebuah soket (socket) menggunakan fungsi `socket()`. Socket ini akan digunakan untuk menerima koneksi dari client. Selanjutnya, opsi soket diatur menggunakan `setsockopt()`.
2. **Mengikat dan Mendengarkan Koneksi**: Socket diikat ke alamat dan port tertentu menggunakan `bind()`. Setelah itu, server mendengarkan (listen) koneksi dari client dengan menggunakan `listen()`.
3. **Menerima Koneksi dari Client**: Server menggunakan `accept()` untuk menerima koneksi baru dari client. Setiap koneksi baru akan memiliki sebuah socket yang dapat digunakan untuk berkomunikasi dengan client tersebut.
4. **Menerima dan Memproses Perintah dari Client**: Setelah koneksi diterima, server membaca perintah yang dikirimkan oleh client menggunakan `read()`. Server kemudian memproses perintah tersebut, misalnya dengan menampilkan data dari file atau melakukan operasi lain sesuai dengan perintah yang diterima.
5. **Memberikan Respons ke Client**: Setelah memproses perintah, server memberikan respons ke client menggunakan `send()`.
6. **Menutup Koneksi**: Setelah selesai berkomunikasi dengan client, server menutup koneksi menggunakan `close()`.

##### Output
![server](gambar/server.png)

## Kendala
- kendala dalam memasukan password jika ingin membuka file.text (file terbuka tanpa memasukan password)
- kesulitan dalam menghapus file "restricted"
- kendala dalam menampilkan webtoon.csv (muncul di client, bukannya server)

## Revisi
- memperbaiki fungsi untuk memindahkan file
- menambahkan program untuk membuka file dengan password
- menambahkan program untuk  menghapus file / folder yang mengandung prefix “restricted”
- menambahkan program untuk menambahkan detail - detail kecil dengan memanfaatkan attribut
-memperbaiki program server.c

# Soal 3
## Deskripsi Program
#### Modul Fuse File System (FS) untuk Modularisasi Direktori

Program ini adalah implementasi Fuse File System (FS) untuk melakukan modularisasi direktori. Modularisasi dilakukan dengan menambahkan awalan "module_" pada nama direktori yang belum dimodularisasi. Selain itu, program ini juga dapat memecah file besar menjadi file-file kecil dengan ukuran file sebesar 1024kb.

# Langkah-langkah Pengerjaan

## 1. Membuat Log File
Tambahkan fungsi `openLogFile` untuk membuka file log sebelum program utama dijalankan.

```c
void openLogFile() {
    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }
    fclose(log_file);
}
```

## 2. Modularisasi Direktori
Tambahkan fungsi `modularisasiDirektori` untuk melakukan modularisasi pada direktori dan sub-direktorinya.

```c
void modularisasiDirektori(const char *dirpath) {
    DIR *dp;
    struct dirent *de;
    struct stat stbuf;

    dp = opendir(dirpath);
    if (dp == NULL) {
        perror("Gagal membuka direktori");
        return;
    }

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) {
            continue;
        }

        char fullpath[1000];
        sprintf(fullpath, "%s/%s", dirpath, de->d_name);

        if (de->d_type == DT_DIR) {
            modularisasiDirektori(fullpath);

            char desc[1000];
            sprintf(desc, "%s", fullpath);
            log_system_call("REPORT", "MODULARIZE_DIR", desc);

            if (strstr(de->d_name, MODULAR_PREFIX) == NULL) {
                char newname[1000];
                sprintf(newname, "%s/%s%s", dirpath, MODULAR_PREFIX, de->d_name);
                rename(fullpath, newname);
            }
        } else {
            if (lstat(fullpath, &stbuf) == 0) {
                if (S_ISREG(stbuf.st_mode) && stbuf.st_size > 1024) {
                    splitFileBesar(fullpath);
                }
            }

            char desc[1000];
            sprintf(desc, "%s", fullpath);
            log_system_call("REPORT", "MODULARIZE_FILE", desc);

            char newname[1000];
            sprintf(newname, "%s/modular_%s", dirpath, de->d_name);
            rename(fullpath, newname);
        }
    }

    closedir(dp);
}
```

## 3. Menangani File Besar
Tambahkan fungsi `splitFileBesar` untuk memecah file besar menjadi file-file kecil dengan ukuran file sebesar 1024 kb

```c
void splitFileBesar(const char *filepath) {
    FILE *original_file = fopen(filepath, "rb");
    if (original_file == NULL) {
        perror("Gagal membuka original file");
        return;
    }

    fseek(original_file, 0, SEEK_END);
    long file_size = ftell(original_file);
    fseek(original_file, 0, SEEK_SET);

    int num_parts = (file_size + 1023) / 1024;

    for (int i = 0; i < num_parts; i++) {
        char part_name[1000];
        sprintf(part_name, "%s.%03d", filepath, i);

        FILE *part_file = fopen(part_name, "wb");
        if (part_file == NULL) {
            perror("Gagal membuka part file");
            fclose(original_file);
            return;
        }

        char buffer[1024];
        int read_size = fread(buffer, 1, 1024, original_file);
        if (read_size < 0) {
            perror("Gagal membaca dari original file");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        if (read_size != 1024) {
            fprintf(stderr, "Error: Read size mismatch\n");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        int write_size = fwrite(buffer, 1, read_size, part_file);
        if (write_size < read_size) {
            perror("Gagal menulis ke part file");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        fclose(part_file);
    }

    fclose(original_file);
}
```

## 4. Menyusun Kembali File Kecil
Tambahkan fungsi `combineFileKecil` untuk menyusun kembali file kecil menjadi file asli.

```c
int combineFileKecil(const char *filepath, char *buf, size_t size) {
    int total_read = 0;
    struct stat stbuf;

    for (int i = 0; ; i++) {
        char part_name[1000];
        sprintf(part_name, "%s.%03d", filepath, i);

        FILE *part_file = fopen(part_name, "rb");
        if (part_file == NULL) {
            break;
        }

        if (total_read < size) {
            int read_size = fread(buf + total_read, 1, size - total_read, part_file);
            total_read += read_size;
        }

        fclose(part_file);

        if (total_read >= size) {
            break;
        }
    }

    if (lstat(filepath, &stbuf) == 0 && total_read < stbuf.st_size) {
        FILE *original_file = fopen(filepath, "rb");
        if (original_file != NULL) {
            fseek(original_file, total_read, SEEK_SET);
            int remaining_read = fread(buf + total_read, 1, stbuf.st_size - total_read, original_file);
            total_read += remaining_read;
            fclose(original_file);
        }
    }

    return total_read;
}
```

## 5. Mencatat Log System Call
Tambahkan fungsi `log_system_call` untuk mencatat log system call sesuai dengan ketentuan.

```c
void log_system_call(const char *level, const char *cmd, const char *desc) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20];
    strftime(timestamp, 20, "%Y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }

    fprintf(log_file, "%s::%04d%02d%02d-%02d:%02d:%02d::%s::%s\n", level,
        tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
        tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, cmd, desc);

    fclose(log_file);
}
```

## 6. Implementasi Fungsi Fuse
Tambahkan implementasi fungsi-fungsi Fuse seperti `xmp_getattr`, `xmp_readdir`, `xmp_rename`, `xmp_rmdir`, `xmp_unlink`, dan `xmp_read` sesuai dengan program yang sudah Anda buat.

**1. xmp_getattr** 
```c
static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;

    if (strstr(path, MODULAR_PREFIX) != NULL) {
        log_system_call("REPORT", "MODULAR", path);
    }

    res = lstat(path, stbuf);
    if (res == -1)
        return -errno;

    if (S_ISDIR(stbuf->st_mode) && strstr(path, MODULAR_PREFIX) != NULL) {
        modularisasiDirektori(path);

        char desc[1000];
        sprintf(desc, "%s", path);
        log_system_call("REPORT", "MODULARIZE_MKDIR", desc);
    }

    return 0;
}
```

**2. xmp_readdir**
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    char desc[MAX_DESC_LENGTH];

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (filler(buf, de->d_name, NULL, 0) != 0)
            break;

        snprintf(desc, sizeof(desc), "%s%s", fpath, de->d_name);
        size_t desc_length = strlen(desc);

        if (desc_length >= sizeof(desc)) {
            fprintf(stderr, "String melebihi batas buffer: %s\n", desc);
        }

        log_system_call("REPORT", "MODULAR", desc);


        int mod_path_len;
        char mod_path[1000];
        mod_path_len = snprintf(mod_path, sizeof(mod_path), "%s%s", fpath, de->d_name);

        if (mod_path_len >= sizeof(mod_path)) {
            // Handle the error.
            fprintf(stderr, "Error in snprintf for mod_path: %s\n", mod_path);
            continue;
        }

        if (strstr(de->d_name, MODULAR_PREFIX) != NULL) {
            modularisasiDirektori(mod_path);
        }
    }

    closedir(dp);
    return 0;
}
```

**3. xmp_rename**
```c
static int xmp_rename(const char *oldpath, const char *newpath) {
    char buf[1000];
    size_t size = sizeof(buf);

    struct stat stbuf;
    if (lstat(oldpath, &stbuf) == -1) {
        return -errno;
    }

    if (!S_ISDIR(stbuf.st_mode)) {
        return 0; 
    }

    if ((stbuf.st_mode & S_IRWXU) != (S_IRWXU | S_IWUSR | S_IXUSR)) {
        return 0; 
    }

    if (strstr(oldpath, MODULAR_PREFIX) != NULL) {
        char desc[1000];
        sprintf(desc, "%s", oldpath);
        log_system_call("REPORT", "REVERT_MODULARIZE", desc);

        char newname[1000];
        int newname_len = snprintf(newname, sizeof(newname), "%s/%s", dirpath, oldpath + strlen(MODULAR_PREFIX));
        if (newname_len > sizeof(newname)) {
            perror("Error reverting modularization");
            return -errno;
        }

        combineFileKecil(newname, buf, size);
        log_system_call("REPORT", "REVERT_MODULARIZE_SUCCESS", desc);
    }

    if (strstr(newpath, MODULAR_PREFIX) != NULL) {
        modularisasiDirektori(newpath);

        char desc[1000];
        sprintf(desc, "%s", newpath);
        log_system_call("REPORT", "MODULARIZE_RENAME", desc);
    }

    return 0;
}
```

**4. xmp_rmdir**
```c
static int xmp_rmdir(const char *path) {
    int res;

    res = rmdir(path);
    if (res == -1)
        return -errno;

    log_system_call("RMDIR", path, "");

    return 0;
}
```

**5. xmp_unlink**
```c
static int xmp_unlink(const char *path) {
    int res;

    res = unlink(path);
    if (res == -1)
        return -errno;

    log_system_call("UNLINK", path, "");

    return 0;
}

```

**6. xmp_read**
```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int fd;
    int res;
    struct stat stbuf;

    (void)fi;

    fd = open(path, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    // Pengecekan apakah direktori memiliki awalan "module_" setelah membaca
    if (strstr(path, MODULAR_PREFIX) != NULL) {
        char desc[1000];
        sprintf(desc, "%s", path);
        log_system_call("REPORT", "DEMODULAR", desc);

        if (lstat(path, &stbuf) == 0) {
            // Memastikan izin dan kepemilikan tetap sama
            chmod(path, stbuf.st_mode);
            chown(path, stbuf.st_uid, stbuf.st_gid);

            // Log saat gagal membaca dari direktori modular
            if (res == -1) {
                char desc_unlink[1000];
                sprintf(desc_unlink, "%s", path);
                log_system_call("FLAG", "UNLINK", desc_unlink);
            }

            // Pemecahan file kecil setelah membaca
            char buf_clean[1024];
            combineFileKecil(path, buf_clean, sizeof(buf_clean));
        }
    }

    return res;
}
```

## 7. Tambahkan fungsi operasi sistem file Fuse
```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .read = xmp_read,
};
```

Fungsi ini mendefinisikan operasi-operasi sistem file Fuse yang akan diimplementasikan oleh program. Setiap operasi memiliki kaitannya dengan fungsi-fungsi yang sudah Anda definisikan sebelumnya dalam program. Berikut adalah penjelasan singkat dari setiap operasi:

- `getattr`: Digunakan untuk mendapatkan atribut dari file atau direktori.
- `readdir`: Digunakan untuk membaca isi dari suatu direktori.
- `rename`: Digunakan untuk me-rename file atau direktori.
- `rmdir`: Digunakan untuk menghapus suatu direktori.
- `unlink`: Digunakan untuk menghapus suatu file.
- `read`: Digunakan untuk membaca data dari suatu file.

## 8. Modifikasi Fungsi Utama
Modifikasi fungsi utama (`main`) untuk memanggil `openLogFile` dan menginisialisasi file system Fuse dengan menggunakan `fuse_main_real`.
```c
int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }
    fclose(log_file);

    return fuse_main_real(argc, argv, &xmp_oper, sizeof(xmp_oper), NULL);
}
```

## 9. Pengecekan Awalan untuk Direktori Modular
Pada fungsi `xmp_getattr`, tambahkan log untuk mencatat direktori modular dengan awalan "module_".

```c
// Tambahkan pada bagian implementasi fungsi xmp_getattr
if (strstr(path, MODULAR_PREFIX)

 != NULL) {
    log_system_call("REPORT", "MODULAR", path);
}
```

## 10. Pengecekan dan Log Saat Membaca Direktori Modular
Pada fungsi `xmp_readdir`, tambahkan log untuk mencatat pembacaan direktori modular dan modularisasi jika ditemukan direktori dengan awalan "module_".

```c
// Tambahkan pada bagian implementasi fungsi xmp_readdir
if (strstr(de->d_name, MODULAR_PREFIX) != NULL) {
    modularisasiDirektori(mod_path);
}
```

## 11. Pengecekan dan Log Saat Me-rename Direktori
Pada fungsi `xmp_rename`, tambahkan log untuk mencatat reversion atau modularisasi saat me-rename direktori.

```c
// Tambahkan pada bagian implementasi fungsi xmp_rename
if (strstr(oldpath, MODULAR_PREFIX) != NULL) {
    // Log untuk reversion
}

if (strstr(newpath, MODULAR_PREFIX) != NULL) {
    // Log untuk modularization
}
```

## 12. Pengecekan dan Log Saat Menghapus Direktori atau File
Pada fungsi `xmp_rmdir` dan `xmp_unlink`, tambahkan log untuk mencatat saat menghapus direktori atau file.

```c
// Tambahkan pada bagian implementasi fungsi xmp_rmdir
log_system_call("RMDIR", path, "");

// Tambahkan pada bagian implementasi fungsi xmp_unlink
log_system_call("UNLINK", path, "");
```

## 13. Pengecekan dan Log Saat Membaca File
Pada fungsi `xmp_read`, tambahkan log untuk mencatat saat membaca dari file dan melakukan demodularisasi.

```c
// Tambahkan pada bagian implementasi fungsi xmp_read
if (strstr(path, MODULAR_PREFIX) != NULL) {
    // Log saat membaca dari direktori modular
}
```

## 14. Pengecekan dan Log Saat Revert Modularisasi
Pada fungsi `xmp_rename`, tambahkan log untuk mencatat saat me-rename dari direktori modular ke direktori non-modular.

```c
// Tambahkan pada bagian implementasi fungsi xmp_rename
if (strstr(oldpath, MODULAR_PREFIX) != NULL) {
    // Log untuk revert modularization
}
```

## 15. Menyusun Kembali File Kecil Setelah Membaca
Pada fungsi `xmp_read`, tambahkan proses untuk menyusun kembali file kecil menjadi file asli setelah membaca dari direktori modular.

```c
// Tambahkan pada bagian implementasi fungsi xmp_read
if (strstr(path, MODULAR_PREFIX) != NULL) {
    // Pemecahan file kecil setelah membaca
    char buf_clean[1024];
    combineFileKecil(path, buf_clean, sizeof(buf_clean));
}
```

### **Script full :**
```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <time.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

// Definisi awalan untuk direktori modular
#define MODULAR_PREFIX "module_"
#define DIR_PATH "/home/znafian/Praktikum-Modul-4_Sisop-2023"
#define LOG_PATH "/home/znafian/Praktikum-Modul-4_Sisop-2023/fs_module.log"
#define XMP_NAMESPACE "http://example.org/fs_module"
#define MAX_DESC_LENGTH 2048

char log_path[1000] = LOG_PATH;
char xmp_namespace[1000] = XMP_NAMESPACE;
char dirpath[1000] = DIR_PATH;

void log_system_call(const char *level, const char *cmd, const char *desc);
void splitFileBesar(const char *filepath);
int combineFileKecil(const char *filepath, char *buf, size_t size);

void modularisasiDirektori(const char *dirpath) {
    DIR *dp;
    struct dirent *de;
    struct stat stbuf;

    dp = opendir(dirpath);
    if (dp == NULL) {
        perror("Gagal membuka direktori");
        return;
    }

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) {
            continue;
        }

        char fullpath[1000];
        sprintf(fullpath, "%s/%s", dirpath, de->d_name);

        if (de->d_type == DT_DIR) {
            modularisasiDirektori(fullpath);

            char desc[1000];
            sprintf(desc, "%s", fullpath);
            log_system_call("REPORT", "MODULARIZE_DIR", desc);

            if (strstr(de->d_name, MODULAR_PREFIX) == NULL) {
                char newname[1000];
                sprintf(newname, "%s/%s%s", dirpath, MODULAR_PREFIX, de->d_name);
                rename(fullpath, newname);
            }
        } else {
            if (lstat(fullpath, &stbuf) == 0) {
                if (S_ISREG(stbuf.st_mode) && stbuf.st_size > 1024) {
                    splitFileBesar(fullpath);
                }
            }

            char desc[1000];
            sprintf(desc, "%s", fullpath);
            log_system_call("REPORT", "MODULARIZE_FILE", desc);

            char newname[1000];
            sprintf(newname, "%s/modular_%s", dirpath, de->d_name);
            rename(fullpath, newname);
        }
    }

    closedir(dp);
}

void splitFileBesar(const char *filepath) {
    FILE *original_file = fopen(filepath, "rb");
    if (original_file == NULL) {
        perror("Gagal membuka original file");
        return;
    }

    fseek(original_file, 0, SEEK_END);
    long file_size = ftell(original_file);
    fseek(original_file, 0, SEEK_SET);

    int num_parts = (file_size + 1023) / 1024;

    for (int i = 0; i < num_parts; i++) {
        char part_name[1000];
        sprintf(part_name, "%s.%03d", filepath, i);

        FILE *part_file = fopen(part_name, "wb");
        if (part_file == NULL) {
            perror("Gagal membuka part file");
            fclose(original_file);
            return;
        }

        char buffer[1024];
        int read_size = fread(buffer, 1, 1024, original_file);
        if (read_size < 0) {
            perror("Gagal membaca dari original file");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        if (read_size != 1024) {
            fprintf(stderr, "Error: Read size mismatch\n");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        int write_size = fwrite(buffer, 1, read_size, part_file);
        if (write_size < read_size) {
            perror("Gagal menulis ke part file");
            fclose(part_file);
            fclose(original_file);
            return;
        }

        fclose(part_file);
    }

    fclose(original_file);
}

// Fungsi untuk membaca file dan menggabungkan file kecil menjadi file asli
int combineFileKecil(const char *filepath, char *buf, size_t size) {
    int total_read = 0;
    struct stat stbuf;

    for (int i = 0; ; i++) {
        char part_name[1000];
        sprintf(part_name, "%s.%03d", filepath, i);

        FILE *part_file = fopen(part_name, "rb");
        if (part_file == NULL) {
            break;
        }

        if (total_read < size) {
            int read_size = fread(buf + total_read, 1, size - total_read, part_file);
            total_read += read_size;
        }

        fclose(part_file);

        if (total_read >= size) {
            break;
        }
    }

    if (lstat(filepath, &stbuf) == 0 && total_read < stbuf.st_size) {
        FILE *original_file = fopen(filepath, "rb");
        if (original_file != NULL) {
            fseek(original_file, total_read, SEEK_SET);
            int remaining_read = fread(buf + total_read, 1, stbuf.st_size - total_read, original_file);
            total_read += remaining_read;
            fclose(original_file);
        }
    }

    return total_read;
}

// Fungsi untuk membuka dan menutup file log sistem
void openLogFile() {
    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }
    fclose(log_file);
}

// Fungsi untuk mencatat log sistem
void log_system_call(const char *level, const char *cmd, const char *desc) {
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20];
    strftime(timestamp, 20, "%Y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }

    fprintf(log_file, "%s::%04d%02d%02d-%02d:%02d:%02d::%s::%s\n", level,
        tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
        tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, cmd, desc);

    fclose(log_file);
}

// Fungsi untuk mencatat properti XMP pada file log
void set_xmp_property(const char *path, const char *level, const char *cmd) {
    xmlDocPtr doc;
    xmlNodePtr root, prop;

    doc = xmlParseFile(log_path);
    if (doc == NULL) {
        perror("Gagal parsing XMP file");
        exit(EXIT_FAILURE);
    }

    root = xmlDocGetRootElement(doc);
    if (root == NULL) {
        perror("Gagal getting root element");
        xmlFreeDoc(doc);
        exit(EXIT_FAILURE);
    }

    xmlNsPtr ns = xmlNewNs(root, BAD_CAST xmp_namespace, NULL);
    xmlSetNs(root, ns);

    prop = xmlNewChild(root, ns, BAD_CAST "LOG", NULL);
    xmlNewProp(prop, BAD_CAST "LEVEL", BAD_CAST level);
    xmlNewProp(prop, BAD_CAST "CMD", BAD_CAST cmd);
    xmlNewProp(prop, BAD_CAST "PATH", BAD_CAST path);

    xmlSaveFormatFile(log_path, doc, 1);

    xmlFreeDoc(doc);
}

// Fungsi untuk mencatat log MODULAR saat xmp_getattr pada level REPORT
static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;

    if (strstr(path, MODULAR_PREFIX) != NULL) {
        log_system_call("REPORT", "MODULAR", path);
    }

    res = lstat(path, stbuf);
    if (res == -1)
        return -errno;

    if (S_ISDIR(stbuf->st_mode) && strstr(path, MODULAR_PREFIX) != NULL) {
        modularisasiDirektori(path);

        char desc[1000];
        sprintf(desc, "%s", path);
        log_system_call("REPORT", "MODULARIZE_MKDIR", desc);
    }

    return 0;
}

// Fungsi untuk membaca direktori
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    char desc[MAX_DESC_LENGTH];

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (filler(buf, de->d_name, NULL, 0) != 0)
            break;

        snprintf(desc, sizeof(desc), "%s%s", fpath, de->d_name);
        size_t desc_length = strlen(desc);

        if (desc_length >= sizeof(desc)) {
            fprintf(stderr, "String melebihi batas buffer: %s\n", desc);
        }

        log_system_call("REPORT", "MODULAR", desc);


        int mod_path_len;
        char mod_path[1000];
        mod_path_len = snprintf(mod_path, sizeof(mod_path), "%s%s", fpath, de->d_name);

        if (mod_path_len >= sizeof(mod_path)) {
            // Handle the error.
            fprintf(stderr, "Error in snprintf for mod_path: %s\n", mod_path);
            continue;
        }

        if (strstr(de->d_name, MODULAR_PREFIX) != NULL) {
            modularisasiDirektori(mod_path);
        }
    }

    closedir(dp);
    return 0;
}

// Fungsi untuk me-rename direktori
static int xmp_rename(const char *oldpath, const char *newpath) {
    char buf[1000];
    size_t size = sizeof(buf);

    struct stat stbuf;
    if (lstat(oldpath, &stbuf) == -1) {
        return -errno;
    }

    if (!S_ISDIR(stbuf.st_mode)) {
        return 0; 
    }

    if ((stbuf.st_mode & S_IRWXU) != (S_IRWXU | S_IWUSR | S_IXUSR)) {
        return 0; 
    }

    if (strstr(oldpath, MODULAR_PREFIX) != NULL) {
        char desc[1000];
        sprintf(desc, "%s", oldpath);
        log_system_call("REPORT", "REVERT_MODULARIZE", desc);

        char newname[1000];
        int newname_len = snprintf(newname, sizeof(newname), "%s/%s", dirpath, oldpath + strlen(MODULAR_PREFIX));
        if (newname_len > sizeof(newname)) {
            perror("Error reverting modularization");
            return -errno;
        }

        combineFileKecil(newname, buf, size);
        log_system_call("REPORT", "REVERT_MODULARIZE_SUCCESS", desc);
    }

    if (strstr(newpath, MODULAR_PREFIX) != NULL) {
        modularisasiDirektori(newpath);

        char desc[1000];
        sprintf(desc, "%s", newpath);
        log_system_call("REPORT", "MODULARIZE_RENAME", desc);
    }

    return 0;
}

static int xmp_rmdir(const char *path) {
    int res;

    res = rmdir(path);
    if (res == -1)
        return -errno;

    log_system_call("RMDIR", path, "");

    return 0;
}

static int xmp_unlink(const char *path) {
    int res;

    res = unlink(path);
    if (res == -1)
        return -errno;

    log_system_call("UNLINK", path, "");

    return 0;
}

// Fungsi untuk membaca file
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int fd;
    int res;
    struct stat stbuf;

    (void)fi;

    fd = open(path, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    // Pengecekan apakah direktori memiliki awalan "module_" setelah membaca
    if (strstr(path, MODULAR_PREFIX) != NULL) {
        char desc[1000];
        sprintf(desc, "%s", path);
        log_system_call("REPORT", "DEMODULAR", desc);

        if (lstat(path, &stbuf) == 0) {
            // Memastikan izin dan kepemilikan tetap sama
            chmod(path, stbuf.st_mode);
            chown(path, stbuf.st_uid, stbuf.st_gid);

            // Log saat gagal membaca dari direktori modular
            if (res == -1) {
                char desc_unlink[1000];
                sprintf(desc_unlink, "%s", path);
                log_system_call("FLAG", "UNLINK", desc_unlink);
            }

            // Pemecahan file kecil setelah membaca
            char buf_clean[1024];
            combineFileKecil(path, buf_clean, sizeof(buf_clean));
        }
    }

    return res;
}

// Fungsi operasi file system Fuse
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .read = xmp_read,
};

// Fungsi utama
int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Gagal membuka log file");
        exit(EXIT_FAILURE);
    }
    fclose(log_file);

    return fuse_main_real(argc, argv, &xmp_oper, sizeof(xmp_oper), NULL);
}
```

### Cara Menjalankan Program

1. Pastikan Anda telah menginstal FUSE (Filesystem in Userspace). Jika belum, Anda dapat menginstalnya dengan menggunakan manajer paket sistem operasi Anda atau mengikuti panduan instalasi resmi FUSE.

2. Compile program menggunakan compiler C. Gunakan perintah berikut pada terminal:
    ```bash
    gcc -o easy easy.c `pkg-config fuse --cflags --libs` -I /usr/include/libxml2 -lxml2
    ```

3. Buat direktori tempat Anda ingin me-mount filesystem. Misalnya:
    ```bash
    mkdir test_fuse
    ```

4. Buat direktori tempat Anda untuk menguji sistem modularisasinya
    ```bash
    mkdir module_test
    ```

5. Buat `file.txt` dengan ukuran file sebesar 3 kb, lalu di path ke direktori `module_test`
    ```bash
    dd if=/dev/zero of=/home/znafian/Praktikum-Modul-4_Sisop-2023/module_test/file bs=1K count=3
    ```

    ![buatfile](gambar/buatfile.png)

6. Jalankan program dengan perintah berikut:
    ```bash
    ./easy test_fuse
    ```   

7. Setelah program dijalankan, cek apakah file `fs_module.log` berhasil terbuat
    ```bash
    ls /path/to/direktoriUtama
    ```

    ![ceklog](gambar/ceklog.png)

8. Ujilah sistem modularisasinya dengan menggunakan command berikut
    ```bash
    ls -lah /path/to/test_fuse
    ```
    lalu akan muncul seperti ini:

    ![isidirektori1](gambar/isidirektori1.png)

9. Lalu cek isi direktori `module_test` apakah `file.txt` tersebut sudah termodularisasi menjadi file-file kecil sebesar 1 kb atau menjadi 3 bagian files
    ```bash
    ls -lah /path/to/module_test
    ```
    jika berhasil, maka akan menjadi seperti ini:

    ![isidirektori2](gambar/isidirektori2.png)

10. Cek isi file `fs_module.log` apakah berhasil menyimpan log user
    ```bash
    nano fs_module.log
    ```
    isi di dalam filenya akan seperti ini:

    ![isilog](gambar/isilog.png)
11. Setelah selesai menggunakan filesystem, lakukan unmount dengan perintah:

    ```bash
    sudo umount test_fuse
    ```
