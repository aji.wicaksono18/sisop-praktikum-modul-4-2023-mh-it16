#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PASSWORD_LENGTH 100

// Fungsi untuk melakukan dekripsi Base64
void base64_decode(const char* input, char* output) {
    const char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int input_length = strlen(input);
    int output_length = 0;

    while (input_length > 0) {
        char chunk[4];
        int i, j;

        // Membaca 4 karakter dari input
        for (i = 0; i < 4; i++) {
            if (input_length > 0) {
                chunk[i] = *input;
                input++;
                input_length--;
            } else {
                chunk[i] = '=';
            }
        }

        // Mendekode 4 karakter menjadi 3 byte
        unsigned char decoded_chunk[3];
        for (i = 0; i < 4; i++) {
            for (j = 0; j < 64; j++) {
                if (chunk[i] == base64_chars[j]) {
                    chunk[i] = j;
                    break;
                }
            }
        }

        decoded_chunk[0] = (chunk[0] << 2) | ((chunk[1] & 0x30) >> 4);
        decoded_chunk[1] = ((chunk[1] & 0x0F) << 4) | ((chunk[2] & 0x3C) >> 2);
        decoded_chunk[2] = ((chunk[2] & 0x03) << 6) | chunk[3];

        // Menyimpan hasil dekripsi ke output
        for (i = 0; i < 3; i++) {
            if (chunk[i + 1] == '=') {
                break;
            }
            output[output_length] = decoded_chunk[i];
            output_length++;
        }
    }

    output[output_length] = '\0';
}

int main() {
    FILE* passFile;
    char password[MAX_PASSWORD_LENGTH];
    char decodedPassword[MAX_PASSWORD_LENGTH];

    passFile = fopen("files/zip-pass.txt", "r");
    if (passFile == NULL) {
        printf("Gagal membuka file zip-pass.txt\n");
        return 1;
    }

    fgets(password, MAX_PASSWORD_LENGTH, passFile);

    password[strcspn(password, "\n")] = 0;

    fclose(passFile);

    base64_decode(password, decodedPassword);

    char unzipCommand[100 + MAX_PASSWORD_LENGTH + 27];
    sprintf(unzipCommand, "unzip -P %s files/home.zip -d unzip", decodedPassword);
    system(unzipCommand);

    return 0;
}