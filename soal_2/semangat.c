#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/xattr.h>
#include <time.h>

#define MAX_PASSWORD_LENGTH 1024

static const char *dirpath = "/home/baebluu/praktikum/sisop/fix2";

void buatLog(const char *status, const char *action, const char *info) {
    time_t t;
    struct tm *tm_now;
    time(&t);
    tm_now = localtime(&t);

    FILE *log_file = fopen("/home/baebluu/praktikum/sisop/fix2/logs-fuse.log", "a");
    if (log_file != NULL) {
        fprintf(log_file, "[%s]::%02d/%02d/%d-%02d:%02d:%02d::%s::%s\n",
                status, tm_now->tm_mday, tm_now->tm_mon + 1, tm_now->tm_year + 1900,
                tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec, action, info);
        fclose(log_file);
    }
}

void pindahFile(const char *sourcePath, const char *destinationPath) {
    struct stat st = {0};

    char destinationDir[512];
    strncpy(destinationDir, destinationPath, sizeof(destinationDir));
    char *lastSlash = strrchr(destinationDir, '/');
    if (lastSlash != NULL) {
        *lastSlash = '\0';
        if (stat(destinationDir, &st) == -1) {
            if (mkdir(destinationDir, 0770) != 0) {
                perror("Error creating destination directory");
                return;
            }
        }
    }

    if (rename(sourcePath, destinationPath) != 0) {
        perror("Error moving file");
        buatLog("FAILED", "pindahFile", sourcePath);
    } else {
        buatLog("SUCCESS", "pindahFile", sourcePath);
    }
}

void buatDirectory(const char *path) {
    struct stat st = {0};
    if (stat(path, &st) == -1) {
        if (mkdir(path, 0770) != 0) {
            perror("Error creating directory");
        } else {
            buatLog("SUCCESS", "Create Directory", path);
        }
    } else {
        buatLog("FAILED", "Create Directory", "Directory already exists");
    }
}

void kategoriFile(const char *fileName) {
    char filepath[512];
    snprintf(filepath, sizeof(filepath), "%s", fileName);

    const char *dot = strrchr(fileName, '.');
    const char *extension = dot ? (dot + 1) : "unknown";

    const char *category;
    if (strcmp(extension, "pdf") == 0 || strcmp(extension, "docx") == 0) {
        category = "documents";
    } else if (strcmp(extension, "jpg") == 0 || strcmp(extension, "png") == 0 || strcmp(extension, "ico") == 0) {
        category = "images";
    } else if (strcmp(extension, "js") == 0 || strcmp(extension, "html") == 0 || strcmp(extension, "json") == 0) {
        category = "website";
    } else if (strcmp(extension, "c") == 0 || strcmp(extension, "sh") == 0) {
        category = "sisop";
    } else if (strcmp(extension, "txt") == 0) {
        category = "text";
    } else if (strcmp(extension, "ipynb") == 0 || strcmp(extension, "csv") == 0) {
        category = "aI";
    } else {
        return;
    }

    char category_folder[32];
    snprintf(category_folder, sizeof(category_folder), "%s", category);

    buatDirectory(category_folder);

    char new_filepath[512];
    snprintf(new_filepath, sizeof(new_filepath), "%s/%s", category_folder, fileName);

    pindahFile(filepath, new_filepath);
}

void FilesByCategory(){
    struct dirent *dir;
    DIR *d;
    d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
            {
                kategoriFile(dir->d_name);
            }
        }
        closedir(d);
    }
}

void checkPassword(const char *password) {
    FILE *passwordFile = fopen("/home/baebluu/praktikum/sisop/fix2/password.bin", "r");
    if (passwordFile == NULL) {
        perror("Error opening password file");
        return; // or take appropriate action
    }

    char storedPassword[MAX_PASSWORD_LENGTH];
    fgets(storedPassword, sizeof(storedPassword), passwordFile);
    fclose(passwordFile);

    // Remove newline character from the password read from the file
    char *newline = strchr(storedPassword, '\n');
    if (newline != NULL) {
        *newline = '\0';
    }

    if (strcmp(password, storedPassword) != 0) {
        printf("Incorrect password\n");
        return -EACCES;
        // or take appropriate action, e.g., deny access or exit the program
    }
}

void generateHtmlFromCsv(const char *csvPath) {
    FILE *csvFile = fopen(csvPath, "r");
    if (csvFile == NULL) {
        perror("Error opening CSV file");
        return;
    }

    char line[1024];
    while (fgets(line, sizeof(line), csvFile) != NULL) {
        char filename[256], title[256], body[512];
        if (sscanf(line, "%[^,],%[^,],%[^\n]", filename, title, body) == 3) {
            // Generate HTML content
            char htmlContent[1024];
            snprintf(htmlContent, sizeof(htmlContent), "<html><head><title>%s</title></head><body>%s</body></html>", title, body);

            // Create HTML file
            char htmlFilename[266]; // (257 + 9 = 266)
            snprintf(htmlFilename, sizeof(htmlFilename), "website/%s", filename);

            FILE *htmlFile = fopen(htmlFilename, "w");
            if (htmlFile != NULL) {
                fprintf(htmlFile, "%s", htmlContent);
                fclose(htmlFile);
            } else {
                perror("Error creating HTML file");
            }
        }
    }

    fclose(csvFile);
}

void ContentCSV() {
    FILE *csv_file = fopen("website/content.csv", "w");
    if (csv_file == NULL) {
        perror("Error creating content.csv");
        return;
    }

    fprintf(csv_file, "file,title,body\n");

    DIR *d;
    struct dirent *dir;
    d = opendir("website");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                char html_path[512];
                snprintf(html_path, sizeof(html_path), "website/%s", dir->d_name);

                FILE *html_file = fopen(html_path, "r");
                if (html_file != NULL) {
                    char line[1024];
                    char title[1024] = "";
                    char body[1024] = "";
                    char filename[1024] = "";
                    strncpy(filename, dir->d_name, sizeof(filename));

                    while (fgets(line, sizeof(line), html_file) != NULL) {
                        char *title_start = strstr(line, "<title>");
                        char *body_start = strstr(line, " <body>");

                        if (title_start != NULL) {
                            strcpy(title, title_start + 7);
                            char *title_end = strstr(title, "</title>");
                            if (title_end != NULL) {
                                *title_end = '\0';
                                char *html_extension = strstr(title, ".html");
                                if (html_extension != NULL) {
                                    *html_extension = '\0';
                                }
                            }
                        }

                        if (body_start != NULL) {
                            strcpy(body, body_start + 6);
                            char *body_end = strstr(body, " </body>");
                            if (body_end != NULL) {
                                *body_end = '\0';
                            }
                        }
                    }

                    fprintf(csv_file, "%s,%s,%s\n", filename, title, body);

                    fclose(html_file);
                }
            }
        }

        closedir(d);
        fclose(csv_file);
    }

    // Generate HTML files from CSV
    generateHtmlFromCsv("website/content.csv");
}


static int smg_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        buatLog("FAILED", "getattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "getattr", path);
    return 0;
}

static int smg_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        buatLog("FAILED", "readdir", path);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    if (res == 0) {
        buatLog("SUCCESS", "readdir", path);
    } else {
        buatLog("FAILED", "readdir", path);
    }

    return 0;
}

static int smg_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    // Check if the file is in the "text" folder
    if (strstr(path, "/text/") != NULL) {
        // Ask for password
        char password[MAX_PASSWORD_LENGTH];
        printf("Enter password: ");
        if (scanf("%s", password) != 1) {
            printf("Error reading password\n");
            return -EACCES; // or appropriate error code
        }

        // Check the password
        checkPassword(password);
        // If the password is incorrect, access will be denied in checkPassword

    }

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        buatLog("FAILED", "read", path);
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        buatLog("FAILED", "read", path);
        res = -errno;
    } else {
        buatLog("SUCCESS", "read", path);
    }

    close(fd);

    return res;
}

static int smg_open(const char *path, struct fuse_file_info *fi) {
    if (strcmp(path, "/website/content.csv") == 0) {
        buatLog("SUCCESS", "open", path);
        return 0;
    } else {
        buatLog("OPEN", "open", path);
        return 0;  // or appropriate error code
    }
}

static int smg_unlink(const char *path) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Check if the file or folder has the "restricted" prefix
    if (strncmp(path, "/restricted", 11) == 0) {
        printf("Deletion of file/folder with 'restricted' prefix is not allowed.\n");
        return -EACCES; // or appropriate error code
    }

    res = unlink(fpath);

    if (res == -1) {
        buatLog("FAILED", "unlink", path);
        return -errno;
    }

    buatLog("SUCCESS", "unlink", path);
    return 0;
}


static int smg_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = setxattr(fpath, name, value, size, flags);

    if (res == -1) {
        buatLog("FAILED", "setxattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "setxattr", path);
    return 0;
}


static int smg_getxattr(const char *path, const char *name, char *value, size_t size) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = getxattr(fpath, name, value, size);

    if (res == -1) {
        buatLog("FAILED", "getxattr", path);
        return -errno;
    }

    buatLog("SUCCESS", "getxattr", path);
    return res;
}


static struct fuse_operations smg_oper = {
    .getattr = smg_getattr,
    .readdir = smg_readdir,
    .read = smg_read,
    .open = smg_open,
    .unlink = smg_unlink,
    .setxattr = smg_setxattr,
    .getxattr = smg_getxattr,
};


int main(int argc, char *argv[]) {

    umask(0);

    FilesByCategory();
    ContentCSV();

    return fuse_main(argc, argv, &smg_oper, NULL);
}
