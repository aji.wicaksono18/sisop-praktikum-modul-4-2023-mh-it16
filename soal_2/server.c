#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#define PORT 8080
#define MAX_BUFFER_SIZE 1024

void sendToClient(int new_socket, const char *message) {
    send(new_socket, message, strlen(message), 0);
}

void readWebtoonData(FILE *webtoonFile, char *result) {
    char line[MAX_BUFFER_SIZE];
    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        strcat(result, line);
    }
}

void showAllTitles(int new_socket) {
    FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
    if (webtoonFile == NULL) {
        sendToClient(new_socket, "Error opening webtoon.csv\n");
        return;
    }

    char result[MAX_BUFFER_SIZE] = "All Titles:\n";
    readWebtoonData(webtoonFile, result);
    sendToClient(new_socket, result);

    fclose(webtoonFile);
}

void showByGenre(int new_socket, const char *genre) {
    FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
    if (webtoonFile == NULL) {
        sendToClient(new_socket, "Error opening webtoon.csv\n");
        return;
    }

    char result[MAX_BUFFER_SIZE] = "";
    char line[MAX_BUFFER_SIZE];

    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        char temp[MAX_BUFFER_SIZE];
        snprintf(temp, sizeof(temp), "%s", line);

        char *tok = strtok(temp, ",");
        if (tok != NULL && strcmp(tok, genre) == 0) {
            strcat(result, line);
        }
    }

    if (strlen(result) == 0) {
        sendToClient(new_socket, "No titles found for the given genre.\n");
    } else {
        sendToClient(new_socket, result);
    }

    fclose(webtoonFile);
}

void showByDay(int new_socket, const char *day) {
    FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
    if (webtoonFile == NULL) {
        sendToClient(new_socket, "Error opening webtoon.csv\n");
        return;
    }

    char result[MAX_BUFFER_SIZE] = "";
    char line[MAX_BUFFER_SIZE];

    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        char temp[MAX_BUFFER_SIZE];
        snprintf(temp, sizeof(temp), "%s", line);

        char *tok = strtok(temp, ",");
        if (tok != NULL && strcmp(tok, day) == 0) {
            strcat(result, line);
        }
    }

    if (strlen(result) == 0) {
        sendToClient(new_socket, "No titles found for the given day.\n");
    } else {
        sendToClient(new_socket, result);
    }

    fclose(webtoonFile);
}

void addToWebtoon(FILE *webtoonFile, const char *data) {
    fseek(webtoonFile, 0, SEEK_END);
    fprintf(webtoonFile, "%s", data);
}

void deleteByTitle(FILE *webtoonFile, const char *title) {
    FILE *tempFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon_temp.csv", "w");
    if (tempFile == NULL) {
        perror("Error creating temporary file");
        return;
    }

    char line[MAX_BUFFER_SIZE];
    while (fgets(line, sizeof(line), webtoonFile) != NULL) {
        char temp[MAX_BUFFER_SIZE];
        snprintf(temp, sizeof(temp), "%s", line);

        char *tok = strtok(temp, ",");
        if (tok != NULL && strcmp(tok, title) != 0) {
            fprintf(tempFile, "%s", line);
        }
    }

    fclose(tempFile);

    // Remove the original file
    remove("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv");

    // Rename the temporary file to the original file name
    rename("/home/baebluu/praktikum/sisop/fix2/aI/webtoon_temp.csv",
           "/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv");
}

// Implementasi lainnya (add, delete, invalid command) disesuaikan dengan kebutuhan

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    // Membuat file descriptor soket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Menetapkan opsi soket
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Mengikat soket ke alamat dan port tertentu
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Menunggu koneksi dari klien
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (1) {
        // Menerima koneksi baru
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        char buffer[MAX_BUFFER_SIZE] = {0};
        read(new_socket, buffer, sizeof(buffer));
        printf("Client: %s\n", buffer);

        // Membaca dan memproses perintah dari client
        if (strcmp(buffer, "show\n") == 0) {
            showAllTitles(new_socket);
        } else if (strncmp(buffer, "genre", 13) == 0) {
            char *genre = strtok(buffer + 13, "\n");
            showByGenre(new_socket, genre);
        } else if (strncmp(buffer, "hari", 12) == 0) {
            char *day = strtok(buffer + 12, "\n");
            showByDay(new_socket, day);
        } else if (strncmp(buffer, "add", 3) == 0) {
            char *data = strtok(buffer + 4, "\n");
            FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "a");
            if (webtoonFile != NULL) {
                addToWebtoon(webtoonFile, data);
                fclose(webtoonFile);
                sendToClient(new_socket, "Data added successfully.\n");
            } else {
                perror("Error opening webtoon.csv\n");
                sendToClient(new_socket, "Error adding data.\n");
            }
        } else if (strncmp(buffer, "delete", 15) == 0) {
            char *title = strtok(buffer + 16, "\n");
            FILE *webtoonFile = fopen("/home/baebluu/praktikum/sisop/fix2/aI/webtoon.csv", "r");
            if (webtoonFile != NULL) {
                deleteByTitle(webtoonFile, title);
                fclose(webtoonFile);
                sendToClient(new_socket, "Data deleted successfully.\n");
            } else {
                perror("Error opening webtoon.csv\n");
                sendToClient(new_socket, "Error deleting data.\n");
            }
        } else {
            sendToClient(new_socket, "Invalid Command\n");
        }

        // Menutup soket setelah selesai
        close(new_socket);
    }

    return 0;
}
